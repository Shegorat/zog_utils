require 'bindata'
require_relative '../custom_strings'

class WChar_t < BinData::Record
  endian  :little

  string  :src, length: 2

  def snapshot
    self.src.force_encoding('UTF-16LE')
  end


end

class MCD_HEADER < BinData::Record
  endian  :little

  uint32  :str_table_offset
  uint32  :str_table_count
  uint32  :sym_codes_offset
  uint32  :sym_codes_count
  uint32  :sym_table_offset
  uint32  :sym_table_count
  uint32  :fnt_table_offset
  uint32  :fnt_table_count
  uint32  :unk_table_offset
  uint32  :unk_table_count
end


class MCD_STR < BinData::Record

end


class MCD_SYM < BinData::Record

end

class MCD_CODE < BinData::Record

end

class MCD_FNT < BinData::Record

end

class MCD_UNK < BinData::Record

end

class MCD_UNKv2 < BinData::Record

end