require 'bindata'

class WChar_t < BinData::Record
  endian :little

  uint16  :raw_val

  def to_a
    [ self.raw_val & 0x00FF, (self.raw_val & 0xFF00) >> 8]
  end

  def to_s
    a = self.to_a
    b = '' << a[0] << a[1]
    c = b.force_encoding(Encoding::BINARY)[0, 2]
    c.force_encoding('UTF-16LE')
  end

  def snapshot
    self.to_s
  end

  def val=(v)
    self.raw_val = v
  end

  def to_i
    self.raw_val
  end
end

class TOTD_FONT < BinData::Record
  endian  :little


  uint32 :d1
  uint32 :u1
  uint32 :id
  uint32 :font_size
  uint32 :d2
  uint32 :d3
  uint32 :d4
  uint32 :d5
  uint32 :u2
  uint32 :u3
  uint32 :table_size
  uint32 :d6
  uint32 :table_count, :value => lambda { self.syms.length }
  uint32 :a1
  uint32 :a2
  uint32 :a3

  array :syms, :initial_length => :table_count do
    WChar_t :s
    uint16  :x
    uint16  :y
    uint8   :w
    uint8   :h
  end

  struct :texture do


    array :data, :type=> :uint8, :initial_length => {}
  end

  def to_text(io)
    io = File.open(io, 'w+b:UTF-16LE') unless io === File

    io.printf("d1=%-4d d2=%-4d d3=%-4d d4=%-4d d5=%-4d d6=%-4d\n",
      d1, d2, d3, d4, d5, d6
    )
    io.printf("u1=%-4d u2=%-4d u3=%-4d\n",
      u1, u2, u3
    )
    io.printf("id=%-4d font_size=%-4d table_size=%-4d table_count=%-4d\n",
      id, font_size, table_size, table_count
    )
    io.printf("a1=%-4d a2=%-4d a3=%-4d\n",
      a1, a2, a3
    )

    io.printf("\n")

    syms.each do |s|
      io.printf("char=%-4d, x=%-3d, y=%-3d, w=%-3d, h=%-3d\n",
        s.s.to_i, s.x, s.y, s.w, s.h
      )

    end
  end

  def from_text(io)
    io = File.open(io, 'rb:UTF-16LE') unless io === File
    reg = Regexp.new('=(\d+)'.encode('UTF-16LE'))

    str1 = io.readline.strip
    str1a = str1.scan(reg).map { |e| e[0].encode('UTF-8').to_i }
    self.d1 = str1a[0]; self.d2 = str1a[1]; self.d3 = str1a[2]; self.d4 = str1a[3]; self.d5 = str1a[4]; self.d6 = str1a[5]

    str2 = io.readline.strip
    str2a = str2.scan(reg).map { |e| e[0].encode('UTF-8').to_i }
    self.u1 = str2a[0]; self.u2 = str2a[1]; self.u3 = str2a[2]

    str3 = io.readline.strip
    str3a = str3.scan(reg).map { |e| e[0].encode('UTF-8').to_i }
    self.id = str3a[0]; self.font_size = str3a[1]; self.table_size = str3a[2]; self.table_count = str3a[3]

    cnt = str3a[3]

    str4 = io.readline.strip
    str4a = str4.scan(reg).map { |e| e[0].encode('UTF-8').to_i }
    self.a1 = str4a[0]; self.a2 = str4a[1]; self.a3 = str4a[2]

    dm = io.readline.strip # dummy

    # print str1.encode('UTF-8'), "\n", str2.encode('UTF-8'), "\n", str3.encode('UTF-8'), "\n", str4.encode('UTF-8'), "\n"

    syms.clear

    cnt.times do
      str = io.readline.strip

      a = str.scan(reg)
      b = a.map { |i| i[0].encode('UTF-8').to_i}

      # print str.encode('UTF-8'), "\n"
      # print a, "\n"
      # print b, "\n"

      c = WChar_t.new(:raw_val => b[0])

      syms << {:s => c, :x => b[1], :y => b[2], :w => b[3], :h => b[4]}
    end
    # puts syms
  end
end

# TODO: Finish code

f = TOTD_FONT.new
io = File.open('C:\1.font')
f.read(io)
puts f
f.to_text('C:\1.font.txt')
f.from_text('C:\1.font.txt')