# Atlus Game, (*.bf) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class BMD_Item < BinData::Record
  endian  :little

  string  :title, length: 24
  int16   :lines_with_sync#, value: -> {strings_sync.length}
  int16   :lines_count#, value: -> {strings.length}

  array   :offsets_sync, initial_length: :lines_with_sync do
    uint32  :offset, value: -> {strings_sync[index].abs_offset - 32}
  end
  uint32  :content_size_sync, value: -> {lines_with_sync > 0 ? strings_sync.num_bytes + 1 : 0}
  array :strings_sync, initial_length: :lines_with_sync do
    UTF8String  :v_start, read_length: 6, asserted_value: "\xF2\x05\xFF\xFF\xF1\x41", byte_align: 4
    UTF8String  :v_raw, read_until: -> {value != nil && value[-2, 2] == "\xF1\x21".b }
  end
  uint8   :dummy_sync, asserted_value: 0, onlyif: -> {lines_with_sync > 0}

  array   :offsets, initial_length: :lines_count, onlyif: -> {lines_count > 0} do
    uint32  :offset, value: -> {strings[index].abs_offset - 32}
  end
  uint32  :content_size, value: -> {lines_count > 0 ? strings.num_bytes + 1 : 0}, onlyif: -> {lines_count > 0}
  array :strings, initial_length: :lines_count, onlyif: -> {lines_count > 0} do
    UTF8String  :v_start, read_length: 6, asserted_value: "\xF2\x05\xFF\xFF\xF1\x41"
    UTF8Stringz  :v_raw#, read_until: -> {value != nil && value[-2, 2] == "\xF1\x21".b }
  end
  uint8   :dummy, asserted_value: 0, onlyif: -> {lines_count > 0}

end

class FLW < BaseInterface
  endian  :little

  uint32  :u1
  uint32  :content_size
  string  :id, read_length: 4
  uint32  :d1
  uint32  :u2
  uint32  :u3
  uint32  :u4




end

BinData::trace_reading do
  # bmd = BMD_Item.new
  # io = File.open('C:\\u1')
  # bmd.read(io)
  # p bmd
  #
  # bmd = BMD_Item.new
  # io = File.open('C:\\u2')
  # bmd.read(io)
  # p bmd
  #
  # bmd = BMD_Item.new
  # io = File.open('C:\\u3')
  # bmd.read(io)
  # p bmd

  bmd = BMD_Item.new
  io = File.open('C:\\u4')
  bmd.read(io)
  p bmd
end