require 'bindata'
require_relative '../custom_strings'

LANG_COUNT = 13

class GB_SUBB < BaseInterface
  endian  :little

  uint32  :a1
  array   :u1, type: :uint32, initial_length: 4
  uint32  :a2
  uint32  :names_count

  array   :names, initial_length: :names_count do
    array   :vals, initial_length: LANG_COUNT do
      UTF8Stringz  :val
      string :al, length: -> { align(al.abs_offset, 4) }
    end
    uint64  :u # hash?
  end

  def export(io)
    self.names.each do |name|
      name.vals.each do |val|
        next if val.val.empty?
        io.puts normalize_string(val.val)
      end
    end
  end

  def import(io)
    self.names_count.times do |i|
      LANG_COUNT.times do |k|
        next if names[i].vals[k].val.empty?
        str =  denormalize_string(io.readline)
        names[i].vals[k].val = str
      end
    end
  end

end

def help
  puts 'Ghostbusters: The Video Game, (*.subb)'
  puts 'Usage:'
  puts 'gb_subb to_plain <source_file> <text_file>'
  puts 'gb_subb import <source_file> <text_file> <new_file>'
  puts 'gb_subb import <source_file> <text_file> -- owerwrite <source_file>'
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    help
    exit -1
  end

  if ARGV[0] == 'to_plain'
    puts 'Convert from binary to plain text..'

    ts = GB_SUBB.new
    ts.read_bin(ARGV[1])
    ts.save_text(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Import plain text to binary..'

    ts = GB_SUBB.new
    ts.read_bin(ARGV[1])
    ts.read_text(ARGV[2])
    ts.save_bin(ARGV[3]) unless ARGV[3] == nil
    ts.save_bin(ARGV[1]) if ARGV[3] == nil
  else
    puts 'Invalid command'
    exit -2
  end

  puts 'All OK..'
end