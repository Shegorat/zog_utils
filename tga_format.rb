require 'bindata'
require 'oily_png'


class RGB555 < BinData::Record
  endian  :little

  bit5    :b
  bit5    :g
  bit5    :r

  def ==(obj)
    obj.is_a?(RGB555) && (self.r == obj.r) && (self.g == obj.g) && (self.b == obj.b)
  end
end

class XRGB1555 < BinData::Record
  endian  :little

  bit5    :b
  bit5    :g
  bit5    :r
  bit1    :x

  def ==(obj)
    obj.is_a?(XRGB1555) && (self.r == obj.r) && (self.g == obj.g) && (self.b == obj.b) && (self.x == obj.x)
  end

end

class RGB888 < BinData::Record
  endian  :little

  uint8   :b
  uint8   :g
  uint8   :r

  def ==(obj)
    obj.is_a?(RGB888) && (self.r == obj.r) && (self.g == obj.g) && (self.b == obj.b)
  end
end

class ARGB8888 < BinData::Record
  endian  :little

  uint8   :b
  uint8   :g
  uint8   :r
  uint8   :a

  def ==(obj)
    obj.is_a?(ARGB8888) && (self.r == obj.r) && (self.g == obj.g) && (self.b == obj.b) && (self.a == obj.a)
  end
end

class TGA < BinData::Record
  endian  :little

  uint8   :id_len, :value => lambda { id.length }
  uint8   :have_palette
  uint8   :image_type

# 1  - palette
# 2  - true color
# 3  - monochrome
# 9  - rle palette
# 10 - rle true color
# 11 - rle monochrome

  uint16  :palette_index
  uint16  :palette_count
  uint8   :palette_bit_count
  uint16  :position_x
  uint16  :position_y
  uint16  :image_width
  uint16  :image_height
  uint8   :image_bit_count
  uint8   :image_flags

  string  :id, :read_length => :id_len, :only_if => lambda { have_id? }

  array   :palette, :initial_length => :palette_count, :only_if => lambda { have_palette? } do
    uint16  :sample
  end

  count_bytes_remaining :bytes_remaining
  string  :data, :read_length => lambda { pixel_count }

  def have_id?
    self.id_len > 0
  end

  def have_palette?
    self.have_palette != 0
  end

  def pixel_count
    self.image_width * self.image_height * self.image_bit_count / 8 if [1, 2, 3].include?(self.image_type)
    self.bytes_remaining if [9, 10, 11].include?(self.image_type)
  end

  def pixels=(px)
    self.data = px
  end

  def init(type, width, height)
    raise 'Invalid image dimensions' if width <= 0 || height <= 0

    self.image_width = width
    self.image_height = height

    case type
      when 1
        self.image_type = type
      when 2
        self.image_type = type
      when 3
        self.image_type = type
      when 9
        self.image_type = type
      when 10
        self.image_type = type
      when 11
        self.image_type = type
      else
        raise 'Invalid image type'
    end
  end

# def initialize
#   self.id_len = 0
#   self.have_palette = 0
#   self.image_type = 0
#   self.palette_index = 0
#   self.palette_count = 0
#   self.palette_bit_count = 0
#   self.position_x = 0
#   self.position_y = 0
#   self.image_width = 0
#   self.image_height = 0
#   self.image_bit_count = 0
#   self.image_flags = 0
#
#   self.id = ''
# end


  def un_rle(source, bcount = 1)
    source = source.bytes
    srclen = source.length
    dest = []
    itr = 0

    while itr < srclen do
      flag = source[itr]

      if flag & 0x80
        n = flag ^ 0x80
        rep = source[itr..itr+bcount]

        n.times do
          dest << rep
        end

        itr += 1 + bcount
      else
        n = flag + 1
        itr = itr.next

        n.times do
          dest << source[itr..itr+bcount]
          itr = itr + bcount
        end
      end
    end

    dest
  end


  def rle(source)
    source = source.bytes
    srclen = source.length
    dest = []
    itr = 0

    while itr < srclen do

      # counting plain data
      n = 0
      while (n < 128) and (n + itr < srclen) and (source[itr + n] != source[itr + n + 1]) do
        n = n.next
      end
      # copy stored data
      if n > 1
        dest << n - 1
        n.times do
          dest << source[itr]
          itr = itr.next
        end
      end

      #counting rle data
      n = 1
      while (n < 128) and (n + itr < srclen) and (source[itr] == source[itr + n]) do
        n = n.next
      end
      #store run length
      if n > 1
        dest << ((n - 1) | 0x80)
        dest << source[itr]
        itr = itr + n
      end

      # next iteration
    end

    dest
  end

  def pixels
    self.data if [1, 2, 3].include?(self.image_type)
    self.un_rle(self.data) if [9, 10, 11].include?(self.image_type)
  end

  def pixel(x, y)
    self[x, y]
  end

  def pixel=(x, y, val)
    self[x, y] = val
  end

  def [](x, y)
    bcount = self.image_bit_count
    height = self.image_height

    idx = (y * height + x) * bcount
    self.pixels[idx]
  end

  def []=(x, y, val)
    bcount = self.image_bit_count
    height = self.image_height

    idx = (y * height + x) * bcount
    self.pixels[idx] = val
  end

  def <<(pixel)

  end





  def to_png(io)

  end

  def from_png(io)

  end

  def read_tga(io)
    io = File.open(io, 'rb') unless io.is_a?(File)
    self.read(io)
  end

  def write_tga(io)
    io = File.open(io, 'w+b') unless io.is_a?(File)
    self.write(io)
  end
end



tga = TGA.new

n = "\x00\x00\x00\x00\x00\x01\x00\x01\x01\x01\x01\x00\x00"
n2 = [4, 0, 1, 1, 0, 3, 1, 0, 0]
p n
p n.bytes

p tga.rle(n)
p n2