require 'bindata'
require_relative '../custom_strings'

class CNV < BaseInterface
  endian  :little

  uint32  :version
  UTF8String  :scene_id, length: 64
  uint32  :triggers_count, value: -> {triggers.length}
  uint32  :messages_count, value: -> {messages.length}
  uint32  :text_size, value: -> {text.num_bytes}

  array :triggers, initial_length: :triggers_count do
    UTF8String  :trigger_id, length: 64
    uint32  :trigger_flags
    uint16  :trigger_index
    UTF8String :trigger_data, length: 126
  end

  array :messages, initial_length: :messages_count do
    UTF8String  :mess_id, length: 64
    uint32  :mess_index
    uint32  :mess_flags
    uint32  :mess_offset, value: -> {text[index].rel_offset}
  end

  array :text, initial_length: :messages_count do
    stringz :str
  end

  def export (io)
    self.text.each do |str|
      io.puts normalize_string(str.encode('UTF-8', 'ISO-8859-1', :replace => '?'))
    end
  end

  def import(io)
    self.messages_count.times do |i|
      str = io.readline.strip
      self.text[i] = denormalize_string(str).encode('Windows-1251', 'UTF-8', :replace => '?')
    end
  end

end

def help
  puts 'Troll and I, (*.cnv) © Shegorat'
  puts 'Usage:'
  puts 'troll_cnv.exe -export <source_file> <text_file>'
  puts 'troll_cnv.exe -import <source_file> <text_file> <new_file>'
  puts 'troll_cnv.exe -import <source_file> <text_file> -- overwrite source file'
end

# TODO: nothing

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      cnv = CNV.new
      cnv.read_bin(ARGV[1])
      cnv.save_text(ARGV[2])
    when '-import'
      puts 'Convert from text to binary.'

      cnv = CNV.new
      cnv.read_bin(ARGV[1])
      cnv.read_text(ARGV[2])
      cnv.save_bin(ARGV[1]) if ARGV[3] == nil
      cnv.save_bin(ARGV[3]) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end