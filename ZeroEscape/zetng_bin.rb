# Zero Escape The Nonary Games (*.bin) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class BIN < BaseInterface
  endian :little

  string  :id, length: 4, assert: -> { value == 'SIR1' }
  uint64  :offtbl_offset, value: -> { strings.rel_offset }
  uint64  :unktbl_offset, value: -> { offset_table.rel_offset }

  buffer  :strings, length: -> { strings_size } do
    array :items, read_until: :eof do
      UTF8Stringz :id
      UTF8Stringz :action
      UTF8Stringz :npc
      UTF8Stringz :message
    end
  end

  buffer  :offset_table, length: -> { offsets_size }, byte_align: 16 do
    array :offsets, read_until: :eof do
      uint64  :id_offset#, value: -> { strings[index].id.abs_offset }
      uint64  :action_offset#, value: -> { strings[index].action.abs_offset }
      uint64  :npc_offset#, value: -> { strings[index].npc.abs_offset }
      uint64  :message_offset#, value: -> { strings[index].message.abs_offset }
    end
  end

  struct  :unk_table, byte_align: 16 do
    uint8 :a1
    uint8 :a2
    uint8 :a3
    uint8 :a4
    uint8 :a5
    uint8 :a6
    stringz :padding
  end

  def strings_size
    n = offtbl_offset - strings.rel_offset
    p 'strings_size', n
    n
  end

  def offsets_size
    n = unktbl_offset - offtbl_offset - strings.rel_offset
    p 'offsets_size', n
    n
  end

  def do_write(io)
    initial_offset = io.offset
    instantiate_all_objs
    @field_objs.each do |f|
      if include_obj?(f)
        if align_obj?(f)
          io.writebytes("\xAA" * bytes_to_align(f, io.offset - initial_offset))
        end
        f.do_write(io)
      end
    end
  end


  def export(io)
    strings.each do |item|
      str = '%s;%s;%s;%s' % [item.id, item.action, item.npc, item.message]
      # p str
      io.puts str
    end
  end

  def import (io)
    strings.length.times do |i|
      str = io.readline.chomp
      s = str.b.split(';').map {|m| m.strip.force_encoding('UTF-8')}
      next if s[0] != strings[i].id
      strings[i].npc = str[2] ? str[2] : ''
      strings[i].message = str[3] ? str[3] : ''
    end
  end

  def append_text(io)
    io = File.open(io, 'ab:UTF-8') unless io.is_a?(File)
    export(io)
  end

end

def help
  puts 'Zero Escape The Nonary Games, (*.dat - SIR1 header) © Shegorat'
  puts 'Usage:'
  puts 'zetng_bin.exe -export <source_file> <text_file>'
  puts 'zetng_bin.exe -import <source_file> <text_file> <new_file>'
  puts 'zetng_bin.exe -import <source_file> <text_file> -- overwrite source file'
end

# TODO: Do import

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      file = BIN.new
      file.read_bin(ARGV[1])
      file.save_text(ARGV[2])
    when '-export_append'
      puts 'Convert from binary to text.'

      file = BIN.new
      file.read_bin(ARGV[1])
      file.append_text(ARGV[2])
    when '-import'
      puts 'Convert from text to binary.'

      file = BIN.new
      p 'read_bin'
      file.read_bin(ARGV[1])
      p 'read_text'
      file.read_text(ARGV[2])
      p 'save_bin'
      file.save_bin(ARGV[1]) if ARGV[3] == nil
      file.save_bin(ARGV[3]) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end