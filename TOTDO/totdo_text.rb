require 'bindata'
require_relative '../custom_strings'

class TOTD_TEXT < BaseInterface
  endian  :little

  uint32 :d1
  uint32 :u1
  uint32 :id
  uint32 :size1
  uint32 :d2
  uint32 :d3
  uint32 :d4
  uint32 :d5
  uint32 :u2
  uint32 :t1_count
  uint32 :d6
  uint32 :d7
  uint32 :d8
  uint32 :tsize
  uint32 :t2_count
  uint32 :d9

  array :table1, :initial_length => :t1_count do
    array   :al, :type => :uint8, :initial_length => lambda {table1[index].abs_offset % 8 > 0 ? 8 - table1[index].abs_offset % 8: 0}
    uint32  :off
    uint32  :len
    uint32  :dm0
    uint32  :id
    uint32  :dm1
  end

  array :table2, :initial_length => :t2_count do
    uint32  :u1
    uint32  :u2
    uint32  :u3
  end

  array :names, :initial_length => :t1_count do
    WStringz :name, :read_length => lambda {table1[index].len + 1}
  end

  def export(io)
    self.names.each do |name|
      next if name.empty?
      # r = Regexp.new("(\r|\n)".encode('UTF-16LE'))
      # name.gsub(r, "\r".encode('UTF-16LE') => '<CR>'.encode('UTF-16LE'), "\n".encode('UTF-16LE') => '<LF>'.encode('UTF-16LE')).strip!
      io.puts normalize_string(name)
    end
  end

  def import(io)
    # r = Regexp.new('(<CR>|<LF>|\n)'.encode('UTF-16LE'))
    table1.each_with_index do |tbl, idx|
      tbl.off = names[idx].abs_offset
      next if names[idx].empty?
      str = WString.new(io.readline)
      # str = str.gsub(r, '<CR>'.encode('UTF-16LE') => "\r".encode('UTF-16LE'), '<LF>'.encode('UTF-16LE') => "\n".encode('UTF-16LE'))
      names[idx] = denormalize_string(str)
      tbl.len = str.length
    end
  end

  def read_text(io)
    super(io, 'rb:UTF-16LE')
  end

  def save_text(io)
    super(io, 'w+b:UTF-16LE')
  end
end

# TODO: Refactor code

def help
  puts 'Typing of the Dead: Overkill (*.stab)'
  puts 'Usage:'
  puts 'totdo_text to_plain <source_file> <text_file>'
  puts 'totdo_text import <source_file> <text_file> <new_file>'
  puts 'totdo_text import <source_file> <text_file> -- overwrite <source_file>'
end

if __FILE__ == $0

  if ARGV.count < 3
    help
    exit
  end

  if ARGV[0] == 'to_plain'
    puts 'Convert from binary to plain text..'

    ts = TOTD_TEXT.new
    ts.read_bin(ARGV[1])
    ts.save_plain(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Import plain text to binary..'

    ts = TOTD_TEXT.new
    ts.read_bin(ARGV[1])
    ts.read_plain(ARGV[2])
    ts.save_bin(ARGV[3]) unless ARGV[3] == nil
    ts.save_bin(ARGV[1]) if ARGV[3] == nil
  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end