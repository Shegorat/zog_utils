require 'bindata'
require_relative '../custom_strings'
require 'colorize'

REPLACE_HASH = Hash["\r" => '<CR>', "\n" => '<LF>', "\x1B" => '<ESC>']

class XL_TYPE_UNI < BaseInterface
  # types = {1, 2, 3, 6, 7, 8}

  mandatory_parameters :item_count, :item_size

  endian :little

  array :offsets, initial_length: -> {item_count * (item_size / 4)} do
    uint32 :off, value: -> {self.strings[index].abs_offset - self.offsets.abs_offset}
  end

  array :strings, initial_length: -> {item_count * (item_size / 4)} do
    UTF8Stringz :str
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i] = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block, REPLACE_HASH) unless block.empty?
    }
  end

end

class XL_TYPE_2_8 < BaseInterface
  # type 2, size 8, flags 0xFFFF0001

  mandatory_parameters :item_count

  endian :little

  array :offsets, :initial_length => :item_count do
    uint32 :ids
    uint32 :off, value: -> {self.strings[index].abs_offset - self.offsets.abs_offset}
  end

  array :strings, :initial_length => :item_count do
    UTF8Stringz :str
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i] = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block, REPLACE_HASH) unless block.empty?
    }
  end

end

class XL_TYPE_3_10 < BaseInterface
  # type 3, size 10, flags 0xFF020001

  mandatory_parameters :item_count

  endian :little

  array :offsets, :initial_length => :item_count do
    uint32 :u1
    uint32 :off, value: -> {self.strings[index].abs_offset - self.offsets.abs_offset}
    uint16 :u2
  end

  array :strings, :initial_length => :item_count do
    UTF8Stringz :str
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i] = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block, REPLACE_HASH) unless block.empty?
    }
  end

end


class XL_TYPE_4_7 < BaseInterface
  # type 4, size 7, flags 0x00030303

  mandatory_parameters :item_count

  endian :little

  array :offsets, :initial_length => :item_count do
    uint8 :u1
    uint8 :u2
    uint8 :u3
    uint32 :off, value: -> {self.strings[index].abs_offset - self.offsets.abs_offset}
  end

  array :strings, :initial_length => :item_count do
    UTF8Stringz :str
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i] = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block, REPLACE_HASH) unless block.empty?
    }
  end

end

class XL_TYPE_6_14 < BaseInterface
  # type 6, size 14, flags 0x02020202

  mandatory_parameters :item_count

  endian :little

  array :offsets, :initial_length => :item_count do
    uint32 :u1
    uint16 :u2
    uint32 :u3
    uint32 :off, value: -> {self.strings[index].abs_offset - self.offsets.abs_offset}
  end

  array :strings, :initial_length => :item_count do
    UTF8Stringz :str
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i] = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block, REPLACE_HASH) unless block.empty?
    }
  end

end

class XL_TYPE_7_18 < BaseInterface
  # type 7, size 18, flags 0x02020202

  mandatory_parameters :item_count

  endian :little

  array :offsets, :initial_length => :item_count do
    uint16 :u1
    uint16 :u2
    uint16 :u3
    uint32 :u4
    uint32 :off1, value: -> {self.strings[index].str1.abs_offset - self.offsets.abs_offset}
    uint32 :off2, value: -> {self.strings[index].str2.abs_offset - self.offsets.abs_offset}
  end

  array :strings, :initial_length => :item_count do
    UTF8Stringz :str1
    UTF8Stringz :str2
  end

  def import(io)
    strings.each_with_index {|str, i|
      strings[i].str1 = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].str1.empty?
      strings[i].str2 = hash_replace(io.gets.chomp, swap_hash(REPLACE_HASH), true) unless strings[i].str2.empty?
    }
  end

  def export(io)
    strings.each {|block|
      io.puts hash_replace(block.str1, REPLACE_HASH) unless block.str1.empty?
      io.puts hash_replace(block.str2, REPLACE_HASH) unless block.str2.empty?
    }
  end

end

class XL_UNKNOWN < BaseInterface
  endian :little

end


TYPE_UNIVERSAL = 1
TYPE_TYPE_2_8 = 2
TYPE_TYPE_3_10 = 3
TYPE_TYPE_4_7 = 4
TYPE_TYPE_6_14 = 5
TYPE_TYPE_7_18 = 6
TYPE_UNKNOWN = 99

class XL_BASE < BaseInterface
  endian :little

  uint32 :id, :assert_value => 0x00134C58

  uint16 :f_size, :value => lambda {self.num_bytes % 65536}
  uint16 :f_type

  uint16 :f_item_count
  uint16 :f_item_size

  uint32 :f_cbsize

  uint32 :f_flags, :onlyif => lambda {self.f_cbsize >= 20}

  array :f_align, :type => :uint8, :initial_length => lambda {self.f_cbsize - 20}

  choice :data, selection: -> {self.selector} do
    XL_TYPE_UNI TYPE_UNIVERSAL, :item_count => :f_item_count, :item_size => :f_item_size
    XL_TYPE_2_8 TYPE_TYPE_2_8, :item_count => :f_item_count
    XL_TYPE_3_10 TYPE_TYPE_3_10, :item_count => :f_item_count
    XL_TYPE_4_7 TYPE_TYPE_4_7, :item_count => :f_item_count
    XL_TYPE_6_14 TYPE_TYPE_6_14, :item_count => :f_item_count
    XL_TYPE_7_18 TYPE_TYPE_7_18, :item_count => :f_item_count
    XL_UNKNOWN TYPE_UNKNOWN
  end

  def selector
    return TYPE_TYPE_2_8 if self.f_type == 2 && self.f_item_size == 8 && self.f_flags == 0xFFFF0001
    return TYPE_TYPE_3_10 if self.f_type == 3 && self.f_item_size == 10 && self.f_flags == 0xFF020001
    return TYPE_TYPE_4_7 if self.f_type == 4 && self.f_item_size == 7 && self.f_flags == 0x00030303
    return TYPE_TYPE_6_14 if self.f_type == 6 && self.f_item_size == 14 && self.f_flags == 0x02020202
    return TYPE_TYPE_7_18 if self.f_type == 7 && self.f_item_size == 18
    return TYPE_UNIVERSAL if self.f_item_size / 4 == self.f_type
    TYPE_UNKNOWN
  end

  def import(io)
    data.import(io)
  end

  def export(io)
    data.export(io)
  end

end


def help
  puts 'KOEI TECMO GAMES, (*.unknown), © Shegorat'.yellow
  puts 'Supported games: '.yellow
  puts "\tDragon Quest Heroes".yellow
  puts "\tDragon Quest Heroes II".yellow
  puts "\tArslan The Warriors of Legend".yellow
  puts "\tWarriors All-Stars".yellow
  puts 'Usage:'.yellow
  puts 'koei_xl.exe -export <source_file> <output_file>'.yellow
  puts 'koei_xl.exe -import <source_file> <text_file> <output_file>'.yellow
  puts 'koei_xl.exe -import <source_file> <text_file> -- overwrite source file'.yellow
end

if __FILE__ == $0
  if ARGV.length < 3
    help
    exit -1
  end

  begin
    case ARGV[0]
      when '-export'
        puts 'Convert from binary to text.'

        xl = XL_BASE.new
        xl.read_bin(ARGV[1])
        xl.save_text(ARGV[2])
      when '-import'
        puts 'Convert from text to binary.'

        xl = XL_BASE.new
        xl.read_bin(ARGV[1])
        xl.read_text(ARGV[2])
        xl.save_bin(ARGV[1]) if ARGV[3] == nil
        xl.save_bin(ARGV[3]) unless ARGV[3] == nil
      else
        puts 'Invalid arguments'.red
        exit -2
    end

  rescue BinData::ValidityError => error
    puts "Invalid input file \"#{ARGV[1]}\", \"#{error.message}\"".red
    exit -2

  rescue Exception => error
    puts "Unknown exception \"#{ARGV[1]}\", \"#{error.message}\"".red
    error.backtrace.each {|trace| puts "\t#{trace}".red}
    exit -2
  end
  puts 'ALL OK'.green
end