require 'bindata'
require_relative '../custom_strings'

REPLACE_HASH = Hash["\r" => '<CR>', "\n" => '<LF>']
ENV['LC_CTYPE']='en_US.UTF-8'

class XL_FILE < BaseInterface
  endian :little

  string  :id, :length => 4, :assert => "XL\x13\x00"
  uint16 :xl_size, value: -> {self.num_bytes & 0xFFFF}
  uint16 :xl_lines
  uint16 :xl_cells
  uint16 :xl_flags
  uint32 :xl_cbsize
  uint32 :xl_dummy1, onlyif: -> {xl_cbsize > 16}
  uint32 :xl_dummy2, onlyif: -> {xl_cbsize > 20}

  array :offsets, initial_length: -> {get_count} do
    uint32 :offset #, :value => lambda {get_offset(index)}
  end

  array :lines, initial_length: -> {get_count} do
    UTF8Stringz :line
  end

  # deprecated
  def get_offset(index)
    lines[index].rel_offset - self.xl_cbsize
  end

  def get_count
    self.xl_lines * self.xl_cells
  end

  # more effective for performance
  def update
    base = self.lines.abs_offset - self.xl_cbsize
    get_count.times do |i|
      self.offsets[i] = base
      base += self.lines[i].num_bytes
    end
  end

  def export (io)
    self.lines.each do |str|
      io.puts hash_replace(str.encode('UTF-8'), REPLACE_HASH)
    end
  end

  def import(io)
    count = get_count
    count.times do |i|
      str = io.readline.chomp('')
      self.lines[i] = hash_replace(str.encode('UTF-8'), REPLACE_HASH, true)
    end
    update
  end

end

def help
  puts 'Dragon Quest II, (*.unknown) © Shegorat'
  puts 'Usage:'
  puts 'bin_xl_v2.exe -export <source_file> <text_file>'
  puts 'bin_xl_v2.exe -import <source_file> <text_file> <new_file>'
  puts 'bin_xl_v2.exe -import <source_file> <text_file> -- overwrite source file'
end

# TODO: nothing

if __FILE__ == $0

  puts ARGV

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      cnv = XL_FILE.new
      cnv.read_bin(ARGV[1].encode('UTF-8'))
      cnv.save_text(ARGV[2].encode('UTF-8'))
    when '-import'
      puts 'Convert from text to binary.'

      cnv = XL_FILE.new
      cnv.read_bin(ARGV[1].encode('UTF-8'))
      cnv.read_text(ARGV[2].encode('UTF-8'))
      cnv.save_bin(ARGV[1].encode('UTF-8')) if ARGV[3] == nil
      cnv.save_bin(ARGV[3].encode('UTF-8')) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end