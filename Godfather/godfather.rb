require 'bindata'

class COTS < BinData::Record
  endian  :little

  struct  :header do
    string  :id, :length => 4, :asserted_value => 'coTS'
    uint32  :u1
    uint8   :c_count
    uint8   :u2
    uint8   :u3
    uint8   :u4
    uint32  :u5
    uint32  :header_size, :value => lambda {header.num_bytes}

    string  :other, :length => lambda {header.header_size - header.other.rel_offset}
  end

  array :chunks, :initial_length => lambda {header.c_count} do
    uint64  :u1
    uint32  :c_size
    uint32  :al_size1
    uint32  :al_size2
    uint32  :u2
  end
end

# TODO: Finish code