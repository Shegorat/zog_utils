require 'bindata'
require_relative '../custom_strings'

class Subtitle < BinData::Record
  endian :little

  array :items, :read_until => :eof do
    uint32  :id

    struct :data, :onlyif => lambda {:id != 0xFFFFFFFF } do
      uint32  :dummy, :value => 0
      uint16  :ts_start
      uint16  :ts_end
      uint16  :str_len, :value => lambda { str.num_bytes }
      uint16  :line_count
      UTF8String  :str, :read_length => :str_len
    end
  end

  def save_to_text(io)
    items.each do |item|
      str = item.data.str.strip.gsub(/([\x00\r\n])/, "\0" => '<x00>', "\r" => '<CR>', "\n" => '<LF>')
      io.printf("{id:%d, tss:%s, tse:%d, lc:%d} = %s\n",
                item.id, item.data.ts_start, item.data.ts_end, item.data.line_count, str)
    end
  end

  def save_to_plain(io)
    items.each do |item|
      str = item.data.str.strip.gsub(/([\x00\r\n])/, "\0" => '<x00>', "\r" => '<CR>', "\n" => '<LF>')
      io.printf("%s\n", str)
    end
  end

  def save_to_plain2(io)
    items.each do |item|
      next if item.data.str.empty?

      str = item.data.str.strip.gsub(/([\x00\r\n])/, "\0" => '<x00>', "\r" => '<CR>', "\n" => '<LF>')
      io.printf("%s\n", str)
    end
  end

  def load_from_text(io)
    io.each do |line|
      str = line.split('=')
      vals = str[0].strip.scan(/:(\d+)/).map {|m| m[0].to_i}
      item = UTF8String.new(str[1].strip.gsub(/(<x00>|<CR>|<LF>)/, '<x00>' => "\0", '<CR>' => "\r", '<LF>' => "\n"))
      len = item.num_bytes
      len2 = (4 - ((len + 1) & 3))
      len3 = len + len2 + 1
      while len < len3 do
        item += "\0"
        len += 1
      end

      items << {:id => vals[0], :data => {:ts_start => vals[1], :ts_end => vals[2], :str_len => len3, :line_count => vals[3], :str => item}}
    end
  end

  def import_from_plain(io)
    items.each do |itm|
      line = io.readline
      item = UTF8String.new(line.strip.gsub(/(<x00>|<CR>|<LF>)/, '<x00>' => "\0", '<CR>' => "\r", '<LF>' => "\n"))
      len = item.num_bytes
      len2 = (4 - ((len + 1) & 3))
      len3 = len + len2 + 1
      while len < len3 do
        item += "\0"
        len += 1
      end

      itm.data.str_len = len3
      itm.data.str = item
    end
  end

  def import_from_plain2(io)
    items.each do |itm|
      next if itm.data.str.empty?

      line = io.readline
      item = UTF8String.new(line.strip.gsub(/(<x00>|<CR>|<LF>)/, '<x00>' => "\0", '<CR>' => "\r", '<LF>' => "\n"))
      len = item.num_bytes
      len2 = (4 - ((len + 1) & 3))
      len3 = len + len2 + 1
      while len < len3 do
        item += "\0"
        len += 1
      end

      itm.data.str_len = len3
      itm.data.str = item
    end
  end

  def read_bin(fname)
    io = File.open(fname, 'rb')
    self.read(io)
  end

  def read_text(fname)
    io = File.open(fname, 'r:UTF-8')
    load_from_text(io)
  end

  def read_plain(fname)
    io = File.open(fname, 'r:UTF-8')
    import_from_plain(io)
  end

  def read_plain2(fname)
    io = File.open(fname, 'r:UTF-8')
    import_from_plain2(io)
  end

  def save_text(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_text(io)
  end

  def save_plain(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_plain(io)
  end

  def save_plain2(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_plain2(io)
  end

  def save_bin(fname)
    io = File.open(fname, 'w+b')
    self.write(io)
    BinData::Uint32le.new(0xFFFFFFFF).write(io)
  end
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    puts 'Too few arguments'
    exit
  end

  if ARGV[0] == 'to_txt'
    puts 'Convert from binary to text..'

    sub = Subtitle.new
    sub.read_bin(ARGV[1])
    sub.save_text(ARGV[2])
  elsif ARGV[0] == 'to_bin'
    puts 'Convert from text to binary..'

    sub = Subtitle.new
    sub.read_text(ARGV[1])
    sub.save_bin(ARGV[2])
  elsif ARGV[0] == 'to_plain'
    puts 'Convert from binary to plain text..'

    sub = Subtitle.new
    sub.read_bin(ARGV[1])
    sub.save_plain(ARGV[2])
  elsif ARGV[0] == 'to_plain2'
    puts 'Convert from binary to plain text..'

    sub = Subtitle.new
    sub.read_bin(ARGV[1])
    sub.save_plain2(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Import text to binary..'

    sub = Subtitle.new
    sub.read_bin(ARGV[1])
    sub.read_plain(ARGV[2])
    sub.save_bin(ARGV[3])
  elsif ARGV[0] == 'import2'
    puts 'Import text to binary..'

    sub = Subtitle.new
    sub.read_bin(ARGV[1])
    sub.read_plain2(ARGV[2])
    sub.save_bin(ARGV[3])
  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end