require 'bindata'
require 'zlib'

class PACFile < BinData::Record

  class PACHEntry < BinDara::Record
    endian  :little

    string  :ph_id, :length => 4

    struct :pach, :onlyif => lambda { is_pach? } do
      uint32  :ph_count

      array   :pach_items, :initial_length => :ph_count do
        uint32  :pi_num, :value => lambda { index }
        uint32  :pi_offset, :value => lambda { pach_data[index].rel_offset }
        uint32  :pi_size, :value => lambda { pach_data[index].num_bytes - pach_data[index].pd_skip.num_bytes }
      end

      array   :pach_data, :initial_length => :ph_count do
        string  :pd_type, :length => 4

        struct :pd_comp_data, :onlyif => lambda { pd_type == 'ZLIB' || pd_type == 'BPE '} do
          uint32  :pd_unk
          uint32  :pd_comp_size, :value => lambda { pd_data.length }
          uint32  :pd_unk_size
          string  :pd_data, :read_length => lambda { pd_comp_size }
        end


        string  :pd_raw_data, :read_length => lambda { pach_items[index].pi_size - 4 },
                :onlyif => lambda { pd_type != 'ZLIB' && pd_type != 'BPE ' }

        skip    :pd_skip, :length => lambda { align4(pd_skip.abs_offset) }
      end
    end

    #string :raw, :onlyif => lambda { !is_pach? }

    skip  :pach_skip, :length => lambda { align2048(pach_skip.abs_offset) }

    def align4(off)
      off2 = (4 - (off & 3))
      off2 = 0 if off2 == 4
      off2
    end

    def align2048(off)
      off2 = ((off / 0x800) + 1) * 0x800
      off3 = off2 - off
      off3
    end

    def is_pach?
      self.ph_id == 'PACH'
    end
  end

  endian :little

  string  :id, :length => 4 # EPK8 || EPAC
  uint32  :list_size, :value => lambda { folder.num_bytes }
  uint32  :data_size, :value => lambda { pach_list.num_bytes }
  uint32  :version, :value => 7

  skip    :hdr_skip, :length => lambda {0x800 - hdr_skip.abs_offset}

  struct  :folder do
    string  :fl_id, :length => 4
    uint16  :fl_count
    uint16  :fl_unk1
    uint32  :fl_unk2

    array   :pac_dir, :initial_length => lambda { pach_count } do
      string	:pd_preid, :length => 4, :onlyif => lambda { is_epk8? }
      string	:pd_id, :length => 4
      uint16	:pd_unk1
      uint8		:pd_unk2
      uint32	:pd_size, :value => lambda { pach_item_size(index) }
      uint8		:pd_unk3
    end
  end

  skip  :folder_skip, :length => lambda {0x4000 - folder_skip.abs_offset}

  array :pach_list, :initial_length => lambda { pach_count } do
    string  :ph_id, :length => 4

    struct  :pach, :onlyif => lambda {ph_id == 'PACH'} do
      uint32  :ph_count, :value => lambda { pach_items.count }

      array :pach_items, :initial_length => :ph_count do
        uint32  :pi_num, :value => lambda { index }
        uint32  :pi_offset, :value => lambda { pach_data[index].rel_offset }
        uint32  :pi_size, :value => lambda { pach_data[index].num_bytes - pach_data[index].pd_skip.num_bytes }
      end

      array :pach_data, :initial_length => :ph_count do
        string  :pd_type, :length => 4

        struct :pd_comp_data, :onlyif => lambda { pd_type == 'ZLIB' || pd_type == 'BPE '} do
          uint32  :pd_unk
          uint32  :pd_comp_size, :value => lambda { pd_data.length }
          uint32  :pd_unk_size
          string  :pd_data, :read_length => lambda { pd_comp_size }

        end
        string  :pd_raw_data, :read_length => lambda { pach_items[index].pi_size - 4 },
                              :onlyif => lambda { pd_type != 'ZLIB' && pd_type != 'BPE ' }

        skip    :pd_skip, :length => lambda { calc_align4(pd_skip.abs_offset) }
      end
    end
    #string :raw_pach, :read_length => lambda {get_raw_pach_size(index)},
    #                  :onlyif => lambda {ph_id != 'PACH'}

    skip  :pach_skip, :length => lambda { skip_calc(pach_skip.abs_offset) }
  end

  string :unknown, :length => 0x800

  def pach_item_size(index)
    self.pach_list[index].num_bytes
  end

  def pach_count
    self.folder.fl_count / 3 if self.is_epac?
    self.folder.fl_count / 4 if self.is_epk8?
  end

  def is_epk8?
    self.id == 'EPK8'
  end

  def is_epac?
    self.id == 'EPAC'
  end

  def skip_calc(off)
    off2 = ((off / 0x800) + 1) * 0x800
    off3 = off2 - off
    puts off
    puts off2
    puts "\n"
    off3
  end

  def calc_align4(off)
    off2 = (4 - (off & 3))
    off2 = 0 if off2 == 4
    off2
  end
  #def get_raw_pach_size(index)
  #  if self.id == 'EPAC'
  #    size = self.folder.pac_dir[index].pd_size
  #  elsif self.id == 'EPK8'
  #    size = self.folder.epk_dir[index].pd_size
  #  end
  #  return size
  #end

end

# TODO: Finish code

pac = PACFile.new
io = File.open('C:\\ltag.pac', 'rb')
pac.read(io)
bio = File.open('C:\\ltag2.pac', 'w+b')
pac.write(bio)

puts pac.folder
#puts pac.pach_list