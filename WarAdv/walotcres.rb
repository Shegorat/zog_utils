require 'bindata'

class RES < BinData::Record
  #auto_call_delayed_io

  endian  :little

  string  :id, :length => 4
  uint32  :data_off
  uint32  :data_size
  uint32  :u3
  uint32  :tags_count
  uint32  :u5
  uint32  :u6
  uint32  :u7

  array   :tags, :initial_length => :tags_count do
    string  :id, :length => 4
    uint32  :u
  end

  array   :records, :initial_length => lambda {record_count} do
    uint32  :i_offset
    uint32  :i_size
    uint32  :u3
    uint32  :u4
    uint32  :u5
  end

  array :items, :initial_length => lambda {record_count} do
    delayed_io :str, :read_abs_offset => lambda {records[index].i_offset + 16  } do
      string :read_length => lambda {records[index].i_size}
    end
  end

  def item_type(i)
    k = i*20 + records.abs_offset - tags.abs_offset
    z = -1
    tags.each do |t|
      break if k < t.u
      z += 1
    end
    x = records[i].i_offset + 16
    puts "#{tags[z].id}, #{x}, #{records[i].i_size}, #{k}"
    tags[z].id
  end

  def open(io)
    io = File.open(io, 'rb') unless io === File
    self.read(io)
  end

  def dump(dir)
    Dir.mkdir(dir) unless Dir.exist?(dir)
    #puts records
    items.each_with_index do |i, k|
      next if records[k].i_size == 0
      type = item_type(k)
      io = File.open("#{dir}\\#{records[k].u4}.#{type}", 'w+b')
      i.read_now!
      #puts i.snapshot
      i.write(io)
    end
  end

  def record_count
    (data_off - records.abs_offset) / 20
  end
end

# TODO: Finish code

if __FILE__ == $0

  if ARGV.count < 3
    puts 'Too dew arguments'
    exit
  end

  if ARGV[0] == 'dump'
    r = RES.new
    r.open(ARGV[1])
    r.dump(ARGV[2])
  elsif ARGV[0] == 'extract'

  elsif ARGV[0] == 'import'
  else
    puts 'Invalid command'
    exit
  end

  puts 'ALL OK..'
end