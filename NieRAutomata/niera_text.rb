require 'bindata'
require_relative '../custom_strings'

class SWString < BinData::Record
  endian  :little

  uint32  :str_len, value: -> { str.length }
  WString :str, read_length: :str_len

  def <<(val)
    self.str = val
  end
end

class TMD < BinData::Record
  endian  :little

  uint32  :string_count, value: -> {strings.length}

  array   :strings, initial_length: :string_count do
    SWString  :title
    SWString  :message
  end

  def to_text(io)
    io = File.open(io, 'w+:UTF-16LE') unless io.is_a?(File)

    self.strings.each do |str|
      io.puts("#{str.title}:: #{normalize_string(str.message)}")
    end
  end

  def plain(io)
    io = File.open(io, 'w+UTF-16LE') unless io.is_a?(File)

    self.strings.each do |str|
      io.puts normalize_string(str.message)
    end
  end

  def to_bin(io)
    io = File.open(io, 'r:UTF-16LE') unless io === File

    io.each_line do |line|
      itms = line.find(/([\w^:]*)::\s*([\w\s]*)/).flatten
      puts itms
      self.strings << {:title => itms[0], :message => denormalize_string(itms[1])}
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-16LE') unless io === File

    self.string_count.times do |i|
      self.strings[i].message = denormalize_string(io.readline)
    end
  end
end

class SMD < BinData::Record
  endian  :little

  uint32  :string_count, value: -> {strings.length}

  array   :strings, initial_length: :string_count do
    WString :title, length: 68
    WString :message, length: 1024
  end

end


class BIN < BinData::Record
  endian  :little

  uint32  :string_count, value: -> {strings.length}
  uint32  :lang_id

  array   :strings, initial_length: :string_count do
    WStringz :str
  end

  def to_text(io)
    io = File.open(io, 'w+b:UTF-16LE') unless io === File

    put_endian(io)
    self.strings.each do |str|
      io.puts normalize_string(str)
    end
  end

  def from_text(io)
    io = File.open(io, 'rb:UTF-16LE') unless io === File
    io.each_line do |line|
      self.strings << denormalize_string(line)
    end
  end

  def from_bin(io)
    io = File.open(io, 'rb') unless io === File
    self.read(io)
  end

  def to_bin(io)
    io = File.open(io, 'w+b') unless io === File
    self.write(io)
  end
end

def help
  puts 'NieR: Automata, (*.smd; *.tmd) © Shegorat'
  puts 'Usage:'
  puts ''
end

# bin = BIN.new
# bin.from_bin('C:\\word_us.bin')
# bin.to_text('C:\\word_us.bin.txt')
# bin2 = BIN.new
# bin2.from_text('C:\\word_us.bin.txt')
# bin2.to_bin('C:\\word_us2.bin')

# TODO: Finish code
# TODO: Refactor code

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-import'

    when '-export'

    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end