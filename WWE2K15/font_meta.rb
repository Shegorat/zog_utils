require 'bindata'
require 'scanf'

class Font_t < BinData::Record
  endian    :little

  uint32    :unk1
  uint32    :unk2

  uint64    :data_size, :value => lambda { self.num_bytes }

  uint32    :unk3
  uint16    :dummy1, :value => 0

  uint32    :flags1
  uint32    :flags2

  uint64    :dummy2,  :value => 0

  uint16    :unk4

  uint16    :scaleW
  uint16    :scaleH

  uint64    :dummy3, :value => 0

  uint32    :unk5
  uint32    :unk6

  uint32    :sym_count, :value => lambda { symbols.count }
  uint32    :sym_offset, :value => lambda { symbols.abs_offset }
  uint64    :dummy4, :value => 0

  uint32    :kern_count, :value => lambda { kerning.count }
  uint32    :kern_offset, :value => lambda { kerning.abs_offset }
  uint64    :dummy5, :value => 0

  uint32    :sym_info_count, :value => lambda { sym_info.count }
  uint32    :sym_info_offset, :value => lambda { sym_info.abs_offset }
  uint64    :dummy6, :value => 0

  uint32    :dummy7, :value => 0

  struct    :extra, :onlyif => lambda { flags1 == 16256 } do
    uint32    :kern_first
    uint32    :kern_last
    uint64    :dummy8, :value => 0
  end

  string    :padding1, :length => lambda { padding1_size }

  array     :symbols, :initial_length => :sym_count do
    uint16    :symbol
  end

  string    :padding2, :length => lambda { padding2_size }

  array     :sym_info, :initial_length => :sym_info_count do
    uint16    :x
    uint16    :y
    uint16    :width
    uint16    :height
    int16     :xoffset
    int16     :yoffset
    int16     :xadvance
    int16     :page
  end

  array     :kerning, :initial_length => :kern_count do
    uint16    :first_s
    uint16    :second_s
    int32     :kern
  end

  def padding1_size
    r = padding1.abs_offset % 16
    0 if r == 0
    16 - r if r != 0
  end

  def padding2_size
    r = padding2.abs_offset % 16
    0 if r == 0
    16 - r if r != 0
  end

  def save_to_text(io)
    io.printf("unknown u1=%-4d u2=%-4d u3=%-4d u4=%-4d u5=%-4d u6=%-4d\n",
    unk1, unk2, unk3, unk4, unk5, unk6
    )
    io.printf("info flags1=%-5d flags2=%-5d scaleW=%-5d scaleH=%-5d\n",
    flags1, flags2, scaleW, scaleH
    )

    io.printf("chars count=%d\n", sym_count)
    sym_count.times  do |i|
      chr = symbols[i]
      info = sym_info[i]
      io.printf("char id=%-4d x=%-5d y=%-5d width=%-5d height=%-5d xoffset=%-5d yoffset=%-5d xadvance=%-5d page=%d\n",
      chr, info.x, info.y, info.width, info.height, info.xoffset, info.yoffset, info.xadvance, info.page
      )
    end
    io.printf("kernings count=%d\n", kern_count)
    kerning.each do |kern|
      io.printf("kerning first=%-3d second=%-3d amount=%d\n",
        kern.first_s, kern.second_s, kern.kern
      )
    end
  end

  def load_from_text(io)

    u = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    self.unk1 = u[0]
    self.unk2 = u[1]
    self.unk3 = u[2]
    self.unk4 = u[3]
    self.unk5 = u[4]
    self.unk6 = u[5]

    i = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    self.flags1 = i[0]
    self.flags2 = i[1]
    self.scaleW = i[2]
    self.scaleH = i[3]

    count = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    vv = Array.new

    count[0].times do
      line = io.readline
      vv << line.scan(/=(\d+)/).map {|m| m[0].to_i}
    end

    vv.sort! {|x, y| x[0] <=> y[0]}
    vv.each do |v|
      symbols.push(v[0])
      sym_info << {:x => v[1], :y => v[2], :width => v[3], :height => v[4], :xoffset => 0, :yoffset => 0, :xadvance => 0, :page => 0}
      #                                                                                 v[5]           v[6]            v[7]        v[8]
    end
    if flags1 == 16256
      extra.kern_first = vv.first[0]
      extra.kern_last = vv.last[0]
    end

    return if io.eof?

    count = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    vv = Array.new

    count[0].times do
      line = io.readline
      vv << line.scan(/=(\d+)/).map {|m| m[0].to_i}
    end

    vv.sort! {|x, y| x[0] <=> y[0]}
    vv.each do |v|
      kerning << {:first_s => v[0], :second_s => v[1], :kern => v[2]}
    end
  end

  def import_from_text(io)
    symbols.clear
    sym_info.clear
    kerning.clear

    l1 = io.readline
    l2 = io.readline
    pg = l2.match(/pages=(\d+)/)[0].sub('pages=', '').to_i
    pages = Array.new
    pg.times do
      pages << io.readline
    end

    count = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    vv = Array.new

    count[0].times do
      line = io.readline
      vv << line.scan(/=(\d+)/).map {|m| m[0].to_i}
    end

    vv.sort! {|x, y| x[0] <=> y[0]}
    vv.each do |v|
      symbols.push(v[0])
      sym_info << {:x => v[1], :y => v[2], :width => v[3], :height => v[4], :xoffset => 0, :yoffset => 0, :xadvance => 0, :page => 0}
      #                                                                                 v[5]           v[6]            v[7]        v[8]
    end
    if flags1 == 16256
      extra.kern_first = vv.first[0]
      extra.kern_last = vv.last[0]
    end

    return if io.eof?

    count = io.readline.scan(/=(\d+)/).map {|m| m[0].to_i}
    vv = Array.new

    count[0].times do
      line = io.readline
      vv << line.scan(/=(\d+)/).map {|m| m[0].to_i}
    end

    vv.sort! {|x, y| x[0] <=> y[0]}
    vv.each do |v|
      kerning << {:first_s => v[0], :second_s => v[1], :kern => v[2]}
    end
  end
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    puts 'Too few arguments'
    exit
  end

  if ARGV[0] == 'to_txt'

    puts 'Convert from bin to text'
    fnt = Font_t.new
    fnt.read(File.open(ARGV[1], 'rb'))
    tio = File.open(ARGV[2], 'wt')
    fnt.save_to_text(tio)

  elsif ARGV[0] == 'to_bin'

    puts 'Convert from text to bin'
    fnt = Font_t.new
    fnt.load_from_text(File.open(ARGV[1], 'rt'))
    bio = File.open(ARGV[2], 'wb')
    fnt.write(bio)

  elsif ARGV[0] == 'import'

    puts 'Reimport font in binary'
    fnt = Font_t.new
    fnt.read(File.open(ARGV[1], 'rb'))
    fnt.import_from_text(File.open(ARGV[2], 'rt'))
    fnt.write(File.open(ARGV[3], 'w+b'))

  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end