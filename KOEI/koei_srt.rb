require 'bindata'

class XL_HEADER < BinData::Record
  endian :little

  uint32 :id
  uint16 :sz
  uint16 :t
  uint32 :u2
  uint32 :u3

end


def sort(source, dest)
  puts File.join(source, '*')

  Dir.mkdir(dest) unless Dir.exists? dest

  map = Hash({})

  Dir.glob(File.join(source, '**', '*')).each {|file|
    next unless File.file? file
    f = File.open(file)
    hdr = XL_HEADER.new
    hdr.read(f)
    f.close

    if hdr.id != 0x00134C58
      puts "#{file} invalid header"
      next
    end

    map[hdr.t] = 0 unless map.has_key? hdr.t

    counter = map[hdr.t]
    map[hdr.t] += 1

    dir = File.join(dest, "type_#{hdr.t}")

    puts "#{file} type #{hdr.t}"

    Dir.mkdir(dir) unless Dir.exists? dir
    File.rename file, File.join(dir, "#{counter}.unknown")
  }

end


def help
  puts 'KOEI TECMO GAMES *.unknown types sorter'
  puts 'koei_srt.exe <source_dir> <output_dir>'
end

if __FILE__ == $0

  if ARGV.length < 2
    help
    exit -1
  end

  puts ARGV

  sort(ARGV[0], ARGV[1])


end