require 'bindata'
require_relative '../custom_strings'

class BIN_Table < BaseInterface
  endian :little

  uint32 :item_count, value: -> { names.length }

  array :offsets, initial_length: :item_count do
    uint32 :offset, value: -> { names.rel_offset + names[index].rel_offset }
    uint32 :len, value: -> { names[index].num_bytes }
  end

  array :names, initial_length: :item_count do
    Stringz :name, read_length: -> { offsets[index].len }
  end

  def push(name)
    self.names << name
  end

  def export(io)
    io.puts "##### Lines count: #{self.item_count} #####"
    self.names.each do |name|
      io.puts normalize_string(name).force_encoding("UTF-8")
    end
  end

  def import(io)
    len = io.readline.scan(/Lines count: (\d+)/).flatten.map { |m| m.to_i }.first
    len.times do
      line = io.readline
      self.push(denormalize_string(line).force_encoding("ASCII-8BIT"))
    end
  end
end

class BIN_Container < BaseInterface
  auto_call_delayed_io

  endian :little

  uint32 :item_count, value: -> { items.length }

  array :offsets, initial_length: :item_count do
    uint32 :offset, value: -> { items.rel_offset + items[index].rel_offset }
  end

  array :items, initial_length: :item_count, byte_align: 16 do
    BIN_Table :bin
    string :al_item, length: -> { align_index(index) }
  end

  def align_index(index)
    align(items[index].al_item.rel_offset, 16)
  end

  def push(bin)
    self.items << {:bin => bin}
  end

  def export(io)
    io.puts "##### Block count: #{self.item_count} #####"
    self.items.each_with_index do |bin, index|
      io.puts "##### Block #{index} of #{self.item_count} #####"
      bin.bin.save_text(io)
    end
  end

  def import(io)
    len = io.readline.scan(/Block count: (\d+)/).flatten.map { |m| m.to_i }.first

    len.times do
      io.readline

      bin = BIN_Table.new
      bin.read_text(io)
      self.push(bin)
    end
  end

end

class BIN_Switcher < BaseInterface
  endian :little

  uint32 :fst
  uint32 :scd

  def container?
    self.scd != -1
  end

  def read_bin(io)
    io = File.open(io, 'rb') unless io.is_a?(File)
    self.read(io)
  end

  def read_text(io, cp = "UTF-8")
    io = File.open(io, "r:#{cp}") unless io.is_a?(File)
    args = io.readline.scan(/Block count: (\d+)/).flatten.map { |m| m.to_i }

    if args.length === 1
      self.fst = args.first
      self.scd = align((self.fst * 4 + 4), 16)
    else
      self.fst = -1
      self.scd = 0
    end

  end
end

class BIN_Table2 < BaseInterface
  auto_call_delayed_io

  endian :little

  uint32 :item_count, value: -> { bins.length }

  array :offsets, initial_length: :item_count do
    uint32 :offset, value: -> { bins.rel_offset + bins[index].rel_offset }
    uint32 :len, value: -> { bins[index].num_bytes }
  end

  array :bins, initial_length: :item_count do
    BIN_Table :bin
  end

  def push(bin)
    self.bins << bin
  end

  def export(io)
    io.puts "##### Bins count: #{self.item_count} #####"
    self.bins.each do |bin|
      bin.save_text(io)
    end
  end

  def import(io)
    len = io.readline.scan(/Bins count: (\d+)/).flatten.map { |m| m.to_i }.first
    len.times do
      bin = BIN_Table.new
      bin.read_text(io)
      self.push(bin)
    end
  end

end

class BIN_Container2 < BaseInterface
  endian :little

  uint32 :item_count, value: -> { items.length }

  array :offsets, initial_length: :item_count do
    uint32 :offset, value: -> { items.rel_offset + items[index].rel_offset }
    uint32 :bin_size, value: -> { items[index].data.num_bytes }
  end

  array :items, initial_length: :item_count do
    string :data, read_length: -> { offsets[index].bin_size }
    string :al_item, length: -> { align_index(index) }
  end

  def align_index(index)
    align(items[index].al_item.rel_offset, 4)
  end

  def push(bin)
    self.items << {:data => bin}
  end

  def dump(io)
    Dir.mkdir(io) unless Dir.exist?(io)

    items.each_with_index do |item, i|
      str = File.join(io, '%08d.bin' % [i])
      puts str
      io2 = File.open(str, 'w+b')
      io2.write(item.data)
    end
  end

  def pack(io)
    raise 'Dir not exists!' unless Dir.exist?(io)

    d = Dir.new(io)
    d.each do |name|
      str = File.join(io, name)
      next if File.directory?(str)

      puts str
      data = File.read(str, mode: 'rb')
      self.push(data)
    end
  end

  def export(io)
    io.puts "##### Block count: #{self.item_count} #####"
    self.items.each_with_index do |bin, index|
      puts
      io.puts "##### Block #{index} of #{self.item_count} #####"
      bin.save_text(io)
    end
  end

  def import(io)
    len = io.readline.scan(/Block count: (\d+)/).flatten.map { |m| m.to_i }.first

    len.times do
      io.readline

      bin = BIN_Table.new
      bin.read_text(io)
      self.push(bin)
    end
  end

end

def help
  puts 'Attack on Titan: Wings of Freedom (*.bin)'
  puts 'Usage:'
  puts 'aot_bin.exe -export <source_file> <text_file>'
  puts 'aot_bin.exe -import <source_file> <bin_file>'
  puts 'aot_bin.exe -export2 <source_file> <dest_dir>'
  puts 'aot_bin.exe -import2 <source_dir> <bin_file>'
end

if __FILE__ == $0

  if ARGV.count < 3
    help
    exit -1
  end

  puts ARGV

  case ARGV[0]
  when '-export' then
    puts 'Convert from binary to text..'

    switcher = BIN_Switcher.new
    switcher.read_bin(ARGV[1])

    bin = BIN_Container.new
    bin = BIN_Table.new unless switcher.container?

    bin.read_bin(ARGV[1])
    bin.save_text(ARGV[2], "ASCII-8BIT")

  when '-import' then
    puts 'Convert from text to binary..'

    switcher = BIN_Switcher.new
    switcher.read_text(ARGV[1], "ASCII-8BIT")

    bin = BIN_Container.new
    bin = BIN_Table.new unless switcher.container?

    bin.read_text(ARGV[1], "ASCII-8BIT")
    bin.save_bin(ARGV[2])

  when '-export2' then
    puts 'Dump binary files..'

    bin = BIN_Container2.new
    bin.read_bin(ARGV[1])
    bin.dump(ARGV[2])
  when '-import2' then
    puts 'Pack binary files..'

    bin = BIN_Container2.new
    bin.pack(ARGV[1])
    bin.save_bin(ARGV[2])
  else
    puts 'Invalid command'
    exit -2
  end

  puts 'All OK..'
end