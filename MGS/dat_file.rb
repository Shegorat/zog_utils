require 'bindata'

class Struct_Header < BinData::Record
  endian  :big

  uint32  :code
  uint8   :a1
  uint8   :a2
  uint16  :a3
  uint16  :cbsize
end

class Strcut_FF40 < BinData::Record
  endian  :big

  uint16  :code
  uint16  :size
  uint32  :u1
  uint16  :u2
  uint16  :u3
  uint16  :u4
  uint16  :u5
end

class Struct_FF10 < BinData::Record
  endian  :big


end

class Struct_FF03 < BinData::Record # msg id
  endian  :big

  uint16  :cbsize, :value => lambda {self.num_bytes}
  uint32  :msg_id
  #string  :dummy, :read_length => lambda {dummy.rel_offset - cbsize}
  uint16  :dummy
end

class Struct_FF02 < BinData::Record
  endian  :big

end

class Struct_FF01 < BinData::Record
  endian  :big

end

class Struct_Parser < BinData::Record
  endian  :big

end


# TODO: Finish code