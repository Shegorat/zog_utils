require 'bindata'
require_relative '../dds_format'

class TEX_Texture < BinData::Record
  endian  :little

  uint32  :txVersion
  array   :txHash, :type => :uint32, :initial_length => 4
  uint32  :txDummy
  uint32  :txFormat
  uint32  :txWidth
  uint32  :txHeight
  uint32  :txUnk1
  uint32  :txMipCount
  uint32  :txUnk2
  uint32  :txUnk3

  count_bytes_remaining :bytes_remaining
  string  :data, :read_length => :bytes_remaining

  def dump(io)
    io = File.open(io, 'w+b') unless io === File

    self.data.write(io)
  end

  def to_dds(io)
    io = File.open(io, 'w+b') unless io === File

    dds = DDS_Texture.new
    dds.data = self.data
    dds.header.dwHeaderFlags = DDS_HEADER_FLAGS_TEXTURE
    if self.txMipCount != 0
      dds.header.dwHeaderFlags |= DDS_HEADER_FLAGS_MIPMAP
      dds.header.dwMipMapCount = self.txMipCount + 1
    end
    dds.header.dwWidth = self.txWidth
    dds.header.dwHeight = self.txHeight
    dds.header.dwSurfaceFlags = DDS_SURFACE_FLAGS_TEXTURE
    dds.header.dwCubemapFlags = 0

    dds.header.dwSurfaceFlags |= DDS_SURFACE_FLAGS_MIPMAP unless self.txMipCount == 0

    case self.txFormat
      when 0x03
        puts 'A8R8G8B8'
        dds.header.ddspf.ddspf_a8r8g8b8
      when 0x04
        puts 'R5G6B5'
        dds.header.ddspf.ddspf_r5g6b5
      when 0x05
        puts 'A4R4G4B4'
        dds.header.ddspf.ddspf_a4r4g4b4
      when 0x17
        puts 'DXT3'
        dds.header.ddspf.ddspf_dxt3
      when 0x2B
        puts 'DXT1'
        dds.header.ddspf.ddspf_dxt1
      when 0x18
        puts 'A8R8G8B8 cubemap'
        dds.header.ddspf.ddspf_a8r8g8b8
        dds.header.dwSurfaceFlags |= DDS_SURFACE_FLAGS_CUBEMAP
        dds.dwCubemapFlags = DDS_CUBEMAP_ALLFACES
      when 0x2E
        puts 'A16B16G16R16F'
        dds.header.ddspf.ddspf_a16b16g16r16f
      when 0x2F
        puts 'A8L8'
        dds.header.ddspf.ddspf_a8l8
      when 0x32
        puts 'DXT5'
        dds.header.ddspf.ddspf_dxt5
      when 0x37
        puts 'L8'
        dds.header.ddspf.ddspf_l8
      else
        puts 'Unknown format'
    end

    dds.write(io)
  end
end

def tex2dds(src, dest)
  puts "Texture: #{src}", "DDS: #{dest}"

  src = File.open(src, 'rb') unless src === File
  dest = File.open(dest, 'w+b') unless dest === File

  tex = TEX_Texture.new
  tex.read(src)
  tex.to_dds(dest)
end

def dds2tex(src1, src2, dest)
  puts "Texture: #{src1}", "DDS: #{src2}", "New texture: #{dest}"

  src1 = File.open(src1, 'rb') unless src1 === File
  src2 = File.open(src2, 'rb') unless src2 === File

  tex = TEX_Texture.new
  tex.read(src1)
  dds = DDS_Texture.new
  dds.read(src2)

  tex.txMipCount = 0
  tex.txMipCount = dds.header.dwMipMapCount - 1 if dds.header.dwMipMapCount > 0
  tex.txWidth = dds.header.dwWidth
  tex.txHeight = dds.header.dwHeight
  tex.data = dds.data

  case dds.header.ddspf.get_type
    when :dds_a8r8g8b8
      if dds.header.dwSurfaceFlags & DDS_SURFACE_FLAGS_CUBEMAP
        puts 'A8R8G8B8 cubemap'
        tex.txFormat = 0x18
      else
        puts 'A8R8G8B8'
        tex.txFormat = 0x03
      end

    when :dds_r5g6b5
      puts 'R5G6B5'
      tex.txFormat = 0x04

    when :dds_a4r4g4b4
      puts 'A4R4G4B4'
      tex.txFormat = 0x05

    when :dds_dxt3
      puts 'DXT3'
      tex.txFormat = 0x17

    when :ddx_dxt1
      puts 'DXT1'
      tex.txFormat = 0x2B

    when :dds_a16b16g16r16f
      puts 'A16B16G16R16F'
      tex.txFormat = 0x2E

    when :dds_a8l8
      puts 'A8L8'
      tex.txFormat = 0x2F

    when :dds_dxt5
      puts 'DXT5'
      tex.txFormat = 0x32

    when :dds_l8
      puts 'L8'
      tex.txFormat = 0x37

    else
      puts 'Unknown format..'
  end

  dest = File.open(dest, 'w+b') unless dest === File
  tex.write(dest)

end

def help
  puts 'Ghostbusters: The Video Game, (*.tex)'
  puts 'Usage:'
  puts 'gb_tex tex2dds <source_file> <dds_file>'
  puts 'gb_tex dds2tex <source_file> <dds_file> <new_file>'
  puts 'gb_tex dds2tex <source_file> <dds_file> -- owerwrite <source_file>'
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    help
    exit
  end

  if ARGV[0] == 'tex2dds'
    puts 'Convert from texture to dds text..'

    tex2dds(ARGV[1], ARGV[2])
  elsif ARGV[0] == 'dds2tex'
    puts 'Import dds intop texture..'

    dds2tex(ARGV[1], ARGV[2], ARGV[3]) unless ARGV[3] == nil
    dds2tex(ARGV[1], ARGV[2], ARGV[1]) if ARGV[3] == nil
  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end