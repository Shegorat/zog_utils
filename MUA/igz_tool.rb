require 'bindata'



class IGZFile < BinData::Record
  endian  :little





  def open(io)
    io = File.open(io, 'rb') unless io === File

    self.read(io)
  end

  def save(io)
    io = File.open(io, 'w+b') unless io === File

    self.write(io)
  end

  def extract_texture(io)
    io = File.open(io, 'w+b') unless io === File

    tga = TGAFile.new
    tga.image_type = 2
    tga.image_width = self.img_width
    tga.image_height = self.img_height
    tga.image_bit_count = 32
    tga.pixels = self.tex_data

    tga.write(io)
  end

  def import_texture(io)
    io = File.open(io, 'rb') unless io === File

    tga = TGAFile.new
    tga.read(io)

    self.img_wodth = tga.image_width
    self.img_height = tga.image_height
    self.tex_data = tga.data
  end
end

# TODO: Finish code

if __FILE__ == $0
  if ARGV.count < 3
    puts 'Too few arguments...'
    exit
  end

  if ARGV[0] == 'extract'
    igz = IGZFile.new
    igz.open(ARGV[1])
    igz.extract_texture(ARGV[2])
  elsif ARGV[0] == 'import'
    igz = IGZFile.new

  else
    puts 'Invalid command...'
    exit
  end

  puts 'All OK...'
end
