require 'bindata'
require 'oily_png'

class DDS_PIXELFORMAT < BinData::Record

  DDS_FOURCC      = 0x00000004  # DDPF_FOURCC
  DDS_RGB         = 0x00000040  # DDPF_RGB
  DDS_RGBA        = 0x00000041  # DDPF_RGB | DDPF_ALPHAPIXELS
  DDS_LUMINANCE   = 0x00020000
  DDS_LUMINANCEA  = 0x00020001

  endian :little

  uint32 :dwSize, value: -> { self.num_bytes }
  uint32 :dwFlags
  string :dwFourCC, length: 4
  uint32 :dwRGBBitCount
  uint32 :dwRBitMask
  uint32 :dwGBitMask
  uint32 :dwBBitMask
  uint32 :dwABitMask

  def make(flags, fourcc, bitcount, rmask, gmask, bmask, amask)
    self.dwFlags = flags
    self.dwFourCC = fourcc
    self.dwRGBBitCount = bitcount
    self.dwRBitMask = rmask
    self.dwGBitMask = gmask
    self.dwBBitMask = bmask
    self.dwABitMask = amask
  end

  def ddspf_dxt1
    self.make(DDS_FOURCC, 'DXT1', 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','1'), 0, 0, 0, 0, 0 };
  end

  def ddspf_dxt2
    self.make(DDS_FOURCC, 'DXT2', 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','2'), 0, 0, 0, 0, 0 };
  end

  def ddspf_dxt3
    self.make(DDS_FOURCC, 'DXT3', 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','3'), 0, 0, 0, 0, 0 };
  end

  def ddspf_dxt4
    self.make(DDS_FOURCC, 'DXT4', 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','4'), 0, 0, 0, 0, 0 };
  end

  def ddspf_dxt5
    self.make(DDS_FOURCC, 'DXT5', 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, MAKEFOURCC('D','X','T','5'), 0, 0, 0, 0, 0 };
  end

  def ddspf_a8r8g8b8
    self.make(DDS_RGBA, "\x00\x00\x00\x00", 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000)

    #{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 32, 0x00ff0000, 0x0000ff00, 0x000000ff, 0xff000000 };
  end

  def ddspf_a1r5g5b5
    self.make(DDS_RGBA, "\x00\x00\x00\x00", 16, 0x00007c00, 0x000003e0, 0x0000001f, 0x00008000)

    #{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00007c00, 0x000003e0, 0x0000001f, 0x00008000 };
  end

  def ddspf_a4r4g4b4
    self.make(DDS_RGBA, "\x00\x00\x00\x00", 16, 0x00000f00, 0x000000f0, 0x0000000f, 0x0000f000)

    #{ sizeof(DDS_PIXELFORMAT), DDS_RGBA, 0, 16, 0x00000f00, 0x000000f0, 0x0000000f, 0x0000f000 };
  end

  def ddspf_r8g8b8
    self.make(DDS_RGB, "\x00\x00\x00\x00", 24, 0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000)

    #{ sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 24, 0x00ff0000, 0x0000ff00, 0x000000ff, 0x00000000 };
  end

  def ddspf_r5g6b5
    self.make(DDS_RGB, "\x00\x00\x00\x00", 16, 0x0000f800, 0x000007e0, 0x0000001f, 0x00000000)

    #{ sizeof(DDS_PIXELFORMAT), DDS_RGB, 0, 16, 0x0000f800, 0x000007e0, 0x0000001f, 0x00000000 };
  end

  def ddspf_a8l8
    self.make(DDS_LUMINANCEA, "\x00\x00\x00\x00", 16, 0x00ff, 0, 0, 0xff00)

    #{ sizeof(DDS_PIXELFORMAT), DDS_LUMINANCEA, 0, 16, 0xff, 0, 0, 0xff00 };
  end

  def ddspf_l8
    self.make(DDS_LUMINANCE, "\x00\x00\x00\x00", 8, 0xff, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_LUMINANCE, 0, 8, 0xff, 0, 0, 0 };
  end

  def ddspf_a16b16g16r16f
    self.make(DDS_FOURCC, "\x71\x00\x00\x00", 0, 0, 0, 0, 0)

    #{ sizeof(DDS_PIXELFORMAT), DDS_FOURCC, 113, 0, 0, 0, 0, 0 };
  end

  def get_type
    case dwFlags
      when DDS_FOURCC
        return :dds_a16b16g16r16f if self.dwFourCC == "\x71\x0\x0\x0"
        return :dds_dxt1 if self.dwFourCC == 'DXT1'
        return :dds_dxt2 if self.dwFourCC == 'DXT2'
        return :dds_dxt3 if self.dwFourCC == 'DXT3'
        return :dds_dxt4 if self.dwFourCC == 'DXT4'
        return :dds_dxt5 if self.dwFourCC == 'DXT5'

      when DDS_RGB
        return :dds_r5g6b5 if self.dwRGBBitCount == 16
        return :dds_r8g8b8 if self.dwRGBBitCount == 24

      when DDS_RGBA
        return :dds_a8r8g8b8 if self.dwRGBBitCount == 32
        return :dds_a1r5g5b5 if self.dwRGBBitCount == 16 and self.dwABitMask == 0x8000
        return :dds_a4r4g4b4 if self.dwRGBBitCount == 16 and self.dwABitMask == 0xf000

      when DDS_LUMINANCEA
        return :dds_a8l8

      when DDS_LUMINANCE
        return :dds_l8

      else
        return :dds_unknown
    end
  end
end

DDS_HEADER_FLAGS_TEXTURE    = 0x00001007  # DDSD_CAPS | DDSD_HEIGHT | DDSD_WIDTH | DDSD_PIXELFORMAT
DDS_HEADER_FLAGS_MIPMAP     = 0x00020000  # DDSD_MIPMAPCOUNT
DDS_HEADER_FLAGS_VOLUME     = 0x00800000  # DDSD_DEPTH
DDS_HEADER_FLAGS_PITCH      = 0x00000008  # DDSD_PITCH
DDS_HEADER_FLAGS_LINEARSIZE = 0x00080000  # DDSD_LINEARSIZE
DDS_SURFACE_FLAGS_TEXTURE   = 0x00001000  # DDSCAPS_TEXTURE
DDS_SURFACE_FLAGS_MIPMAP    = 0x00400008  # DDSCAPS_COMPLEX | DDSCAPS_MIPMAP
DDS_SURFACE_FLAGS_CUBEMAP   = 0x00000008  # DDSCAPS_COMPLEX
DDS_CUBEMAP_POSITIVEX       = 0x00000600  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEX
DDS_CUBEMAP_NEGATIVEX       = 0x00000a00  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEX
DDS_CUBEMAP_POSITIVEY       = 0x00001200  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEY
DDS_CUBEMAP_NEGATIVEY       = 0x00002200  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEY
DDS_CUBEMAP_POSITIVEZ       = 0x00004200  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_POSITIVEZ
DDS_CUBEMAP_NEGATIVEZ       = 0x00008200  # DDSCAPS2_CUBEMAP | DDSCAPS2_CUBEMAP_NEGATIVEZ
DDS_CUBEMAP_ALLFACES        = DDS_CUBEMAP_POSITIVEX | DDS_CUBEMAP_NEGATIVEX | DDS_CUBEMAP_POSITIVEY | DDS_CUBEMAP_NEGATIVEY | DDS_CUBEMAP_POSITIVEZ | DDS_CUBEMAP_NEGATIVEZ
DDS_FLAGS_VOLUME            = 0x00200000  # DDSCAPS2_VOLUME

class DDS_HEADER < BinData::Record
  endian  :little

  string :dwID, value: 'DDS ', length: 4
  uint32 :dwSize, value: -> { self.num_bytes - 4 }
  uint32 :dwHeaderFlags
  uint32 :dwHeight
  uint32 :dwWidth
  uint32 :dwPitchOrLinearSize
  uint32 :dwDepth # only if DDS_HEADER_FLAGS_VOLUME is set in dwHeaderFlags
  uint32 :dwMipMapCount
  array  :dwReserved1, type: :uint32, length: 11
  DDS_PIXELFORMAT :ddspf
  uint32 :dwSurfaceFlags
  uint32 :dwCubemapFlags
  array  :dwReserved2, type: :uint32, length: 3
end

class DDS_Texture < BinData::Record
  endian  :little

  DDS_HEADER  :header

  count_bytes_remaining :bytes_remaining
  string      :data, :read_length => :bytes_remaining

  def data=(data)
    self.data = data
  end

  def tex_height=(val)
    self.header.dwHeight = val
  end

  def tex_width=(val)
    self.header.dwWidth = val
  end

  def dxt1!
    self.header.ddspf.ddspf_dxt1
  end

  def dxt2!
    self.header.ddspf.ddspf_dxt2
  end

  def dxt3!
    self.header.ddspf.ddspf_dxt3
  end

  def dxt4!
    self.header.ddspf.ddspf_dxt4
  end

  def dxt5!
    self.header.ddspf.ddspf_dxt5
  end

  def a8r8g8b8!
    self.header.ddspf.ddspf_a8r8g8b8
  end

  def a1r5g5b5!
    self.header.ddspf.ddspf_a1r5g5b5
  end

  def a4r4g4b4!
    self.header.ddspf.ddspf_a4r4g4b4
  end

  def r8g8b8!
    self.header.ddspf.ddspf_r8g8b8
  end

  def r5g6b5!
    self.header.ddspf.ddspf_r5g6b5
  end

  def a8l8!
    self.header.ddspf.ddspf_a8l8
  end

  def l8!
    self.header.ddspf.ddspf_l8
  end

  def a16b16g16r16f!
    self.header.ddspf.ddspf_a16b16g16r16f
  end


  def save_png(io)

  end

  def save_dds(io)
    io = File.open(io, 'w+b') unless io.is_a?(File)
    self.write(io)
  end

  def read_dds(io)
    io = File.open(io, 'rb') unless io.is_a?(File)
    self.read(io)
  end
end