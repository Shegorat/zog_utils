# Operation Abyss New Tokyo Legacy, (*.dat) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class DAT < BaseInterface
  endian  :little

  uint32  :item_count, value: -> { items.count }

  array   :offs, initial_length: :item_count do
    uint32  :off, value: -> { items.rel_offset + items[index].rel_offset }
  end

  array   :items, initial_length: :item_count do
    uint16  :stage
    uint16  :str_count, value: -> { strings.length - 1 }
    uint16  :self_size, value: -> { str_offsets.num_bytes + 6 }

    array   :str_offsets, initial_length: -> { str_count + 1 } do
      uint16  :off, value: -> { index == 0 ? 0 : (strings.rel_offset + strings[index].rel_offset) }
    end

    array   :strings, initial_length: -> { str_count + 1 } do
      UTF8Stringz :str
    end
    virtual :b_align, byte_align: 4
  end

  def fallback(sym)
    p sym
  end

  def export(io)
    items.each do |item|
      item.strings.each do |str|
        rex = Regexp.new '(\x81[\x46\x67\x68\x7E\x9B\x9F\xA0\xA2\xCB]|\x87[\x40-\x49\x5F]|\x40\x33\x72|\x40\x30\x72)', nil, 'n'

        str2 = str.chomp.b.gsub(rex) {|s|
          case (s)
            when "\x40\x33\x72"
              '<B>'
            when "\x40\x30\x72"
              '</B>'
            else
              d = s.bytes.map {|a| 'x%2X' % a}.join('')
              "<#{d}>"
          end
        }.force_encoding('UTF-8')

        io.puts str2 unless str2.empty?
      end
    end
  end

  def import(io)
    item_count.times do |i|
      t = items[i].str_count + 1
      t.times do |k|
        old = items[i].strings[k].chomp
        unless old.empty?
          str = io.readline.chomp

          rex = Regexp.new '(<B>|<\/B>|<[xA-Z0-9]+>)', nil, 'n'
          str2 = str.b.gsub(rex) { |s|
            case s.b
              when '<B>'
                d = "\x40\x33\x72"
              when '</B>'
                d = "\x40\x30\x72"
              else
                rex2 = Regexp.new 'x([\dA-Z]+)', nil, 'n'
                d = s.scan(rex2).flatten.map { |a| a.hex.chr }.join
            end
            d
          }.force_encoding('UTF-8')

          items[i].strings[k] = str2
        end
      end
    end
  end
end

def help
  puts 'Operation Abyss New Tokyo Legacy, (*.dat) © Shegorat'
  puts 'Usage:'
  puts 'oantl_dat.exe -export <source_file> <text_file>'
  puts 'oantl_dat.exe -import <source_file> <text_file> <new_file>'
  puts 'oantl_dat.exe -import <source_file> <text_file> -- overwrite source file'
end

# TODO: ALL OK

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      file = DAT.new
      file.read_bin(ARGV[1])
      file.save_text(ARGV[2])
    when '-import'
      puts 'Convert from text to binary.'

      file = DAT.new
      file.read_bin(ARGV[1])
      file.read_text(ARGV[2])
      file.save_bin(ARGV[1]) if ARGV[3] == nil
      file.save_bin(ARGV[3]) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end