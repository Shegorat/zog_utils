require 'bindata'
require_relative './custom_strings'
require 'json'
require 'colorize'
require 'csv'

REPLACE_HASH = Hash["\r" => '<CR>', "\n" => '<LF>', "\x1B" => '<ESC>', "\r\n" => '<CF>']

def sync_spaces(in1, in2, out)
  inf = File.open(in1, 'r')
  inf2 = File.open(in2, 'r')
  outf = File.open(out, 'w+')

  inf.each do |line|
    if !line.strip.empty?
      line2 = inf2.readline
      outf.printf("%s\n", line2.strip)
    else
      outf.printf("\n")
    end

  end
end

def load_dict(dict_dir1, dict_dir2)
  dict = Hash.new('')

  Dir.glob(File.join('**', dict_dir1, '**', '*.txt')).each do |file|
    file1 = file.sub(dict_dir1, dict_dir2)
    dc1 = File.open(file, 'r:UTF-8')
    dc2 = File.open(file1, 'r:UTF-8')
    dc1.each_line do |line|
      break if dc2.eof?
      line2 = dc2.readline
      dict[line.chomp] = line2.chomp
    end
  end

  dict
end

def print_dict(hash, file)
  File.open(file, "w+") {|f|
    hash.each {|key, value|
      f.puts "\"#{key}\": \"#{value}\""
    }
  }
end

def dict_translate(dict_dir1, dict_dir2, source_dir, dest_dir)
  dict = load_dict(dict_dir1, dict_dir2)

  # print_dict(dict, "dict_dump.txt")

  Dir.mkdir(dest_dir) unless Dir.exist?(dest_dir)

  Dir.glob(File.join('**', source_dir, '**', '*.txt')).each do |file|
    file1 = file.sub(source_dir, dest_dir)
    dc1 = File.open(file, 'r:UTF-8')
    dc2 = File.open(file1, 'w+:UTF-8')
    dc1.each_line do |line|
      line = line.chomp
      line2 = dict[line].chomp
      dc2.printf("%s\n", line2) if !line2.empty? and !line.empty?
      dc2.printf("%s\n", line) if line2.empty?
    end
  end
end


def json_beautify(src, dest)
  begin
    src = File.open(src, 'rb:UTF-8') unless src.is_a?(File)
    hash = JSON.load(src)
    dest = File.open(dest, 'w+b:UTF-8') unless dest.is_a?(File)
    json = JSON.pretty_generate(hash)
    dest.write(json)
  rescue JSON::ParserError => error
    puts "Error: #{error.class} in".red, error.backtrace, error.message[0, 256]
    exit -99
  end
end

def json_minify(src, dest)
  begin
    src = File.open(src, 'rb:UTF-8') unless src.is_a?(File)
    hash = JSON.load(src)
    dest = File.open(dest, 'w+b:UTF-8') unless dest.is_a?(File)
    json = JSON.generate(hash)
    dest.write(json)
  rescue JSON::ParserError => error
    puts "Error: #{error.class} in".red, error.backtrace, error.message[0, 256]
    exit -99
  end

end

def map_replace(src, dest, hash, reverse = false)
  begin
    hash = File.open(hash, 'r:UTF-8') unless hash.is_a?(File)
    src = File.open(src, 'r:UTF-8') unless src.is_a?(File)
    dest = File.open(dest, 'w+b:UTF-8') unless dest.is_a?(File)

    if hash == nil || dest == nil || src == nil
      puts 'Invalid arguments'.red
      exit -2
    end

    map = create_replace_map(hash, reverse)

    if map.empty?
      puts 'Empty hash table'.yellow
      exit -3
    end

    n = 0
    src.each_line do |line|
      dest.puts hash_replace(line.chomp, map, reverse)
      n = n.next
    end

    puts "Processed #{n} lines".green
  rescue StandardError => error
    puts "Error: #{error}".red
    exit -99
  end
end


def drop(src, dest, mask)
  begin
    mask = File.open(mask, 'r:UTF-8') unless mask.is_a?(File)
    dest = File.open(dest, 'w+:UTF-8') unless dest.is_a?(File)
    src = File.open(src, 'r:UTF-8') unless src.is_a?(File)

    if mask == nil || dest == nil || src == nil
      puts 'Invalid arguments'.red
      exit -2
    end

    src_data = src.read.b
    rex = mask.map {|l| l.chomp}.join('|')
    rx = Regexp.new rex.strip, nil, 'n'
    matches = src_data.scan(rx)

    if matches == nil || matches.empty?
      puts 'No matches founded.'.yellow
      exit 0
    end

    matches.flatten!.map! {|x| x.chomp.b}
    matches.each {|m| dest.puts m.force_encoding('UTF-8')}

    puts "Founded #{matches.length} matches".green
  rescue StandardError => error
    puts "Error: #{error}".red
    exit -99
  end
end

def inject(src, dest, mask, replace)
  begin
    src = File.open(src, 'r:UTF-8') unless src.is_a?(File)
    dest = File.open(dest, 'w+:UTF-8') unless dest.is_a?(File)
    mask = File.open(mask, 'r:UTF-8') unless mask.is_a?(File)
    replace = File.open(replace, 'r:UTF-8') unless replace.is_a?(File)

    if mask == nil || dest == nil || src == nil || replace == nil
      puts 'Invalid arguments'.red
      exit -2
    end

    if replace.size <= 0
      puts 'Empty replacement file'.red
      exit -3
    end

    src_data = src.read.b
    rex = mask.map {|l| l.chomp}.join('|')
    rx = Regexp.new rex.strip, nil, 'n'
    counter = 0

    new_data = src_data.gsub(rx) {|match|
      counter += 1
      replace.gets.b.chomp
    }

    dest.write(new_data.force_encoding('UTF-8'))

    puts "Replaced #{counter} matches".green
  rescue Exception => error
    puts "Error: #{error}".red
    exit -99
  end
end

WINDOWS_PATH_INVALID_CHARS = %w(\ / ? < > : | ")

def csv_drop(src, dest, argv)
  if src == nil || dest == nil
    puts 'Invalid arguments'.red
    exit -1
  end

  unless File.exists? src
    puts 'Invalid input file'.red
    exit -2
  end

  drop_columns = []
  print_columns = false
  info_only = false
  force_quotes = false
  liberal_parsing = false
  delimiter = ','

  unless argv == nil && argv.empty?
    argv.each {|param|
      # drop columns array
      if param.start_with? '-cn='
        arr = param[4..-1]
        drop_columns = arr.split(',').map {|v| v.strip.to_sym}
      end

      # print columns option
      print_columns = true if param == '-pc' || param == '-i'

      # info only mode
      info_only = true if param == '-i'

      # change delimiter
      delimiter = unescape(param[4..param.length]) if param.start_with? '-dl='

      # force quote mode
      force_quotes = true if param == '-fq'

      # liberal parsing
      liberal_parsing = true if param == '-lp'
    }
  end

  counter = 0
  table = CSV.table(src, {return_headers: false,
                          # row_sep: "\r\n",
                          col_sep: delimiter,
                          force_quotes: force_quotes,
                          liberal_parsing: liberal_parsing,
                          header_converters: nil,
                          converters: nil
  })

  if print_columns
    puts 'CSV columns:'
    table.headers.each {|header|
      puts "  #{header.inspect}"
    }
  end

  return if info_only

  dest = dest.encode('UTF-8') unless dest.encoding == 'UTF-8'
  Dir.mkdir(dest) unless Dir.exists? dest

  table.by_col.each {|pair|
    next if pair == nil || pair.empty?

    column = pair.first

    unless drop_columns.empty?
      next unless drop_columns.include? column
    end

    if column == nil || column.empty? || WINDOWS_PATH_INVALID_CHARS.reduce(false) {|memo, char| memo || column.include?(char)}
      puts "Invalid column name '#{column.inspect}'".light_red
      next
    end

    filename = File.join(dest, "#{column.to_s}.txt").force_encoding('UTF-8')
    puts filename
    out = File.open(filename, 'w+:UTF-8')

    pair.last.each {|row|
      next if row == nil || (row.is_a?(String) && row.empty?)
      counter += 1
      out.puts hash_replace(row, REPLACE_HASH)
    }
  }

  puts "Dropped #{counter} lines".green
end

def csv_inject(src, dest, argv)
  if src == nil || dest == nil
    puts 'Invalid arguments'.red
    exit -1
  end

  unless File.exists? src
    puts 'Invalid input file'.red
    exit -2
  end

  drop_columns = []
  print_columns = false
  info_only = false
  force_quotes = false
  delimiter = ','
  liberal_parsing = false

  unless argv == nil && argv.empty?
    argv.each {|param|
      # drop columns array
      if param.start_with? '-cn='
        arr = param[4..-1]
        drop_columns = arr.split(',').map {|v| v.strip.to_sym}
      end

      # print columns option
      print_columns = true if param == '-pc' || param == '-i'

      # info only mode
      info_only = true if param == '-i'

      # change delimiter
      delimiter = unescape(param[4..param.length]) if param.start_with? '-dl='

      # force quote mode
      force_quotes = true if param == '-fq'

      # liberal parsing
      liberal_parsing = true if param == '-lp'
    }
  end

  counter = 0
  table = CSV.table(src, {return_headers: false,
                          # row_sep: "\r\n",
                          col_sep: delimiter,
                          force_quotes: force_quotes,
                          header_converters: nil,
                          liberal_parsing: liberal_parsing,
                          converters: nil
  })

  if print_columns
    puts 'CSV columns:'
    table.headers.each {|header|
      puts "\t#{header.inspect}"
    }
  end

  return if info_only

  dest = dest.encode('UTF-8') unless dest.encoding == 'UTF-8'
  table.by_col.each {|pair|
    next if pair == nil || pair.empty?

    column = pair.first

    unless drop_columns.empty?
      next unless drop_columns.include? column.to_sym
    end

    if column == nil || column.empty? || WINDOWS_PATH_INVALID_CHARS.reduce(false) {|memo, char| memo || column.include?(char)}
      puts "Invalid column name '#{column.inspect}'".light_red
      next
    end

    arr = pair.last

    filename = File.join(dest, "#{column.to_s}.txt").encode('UTF-8')
    next unless File.exists? filename

    inf = File.open(filename, 'r:UTF-8')
    arr.each_with_index {|val, idx|
      next if val == nil || val.empty?
      counter += 1
      arr[idx] = hash_replace(inf.gets, swap_hash(REPLACE_HASH))
    }
  }

  new_dest = File.open(src, 'w+:UTF-8')
  new_dest.write(table.to_csv)
  new_dest.close

  puts "Injected #{counter} lines".green
end

def dedup(source, dest)
  source = File.open(source, 'r:UTF-8') unless source.is_a?(File)

  map = Array[]
  total_lines = 0

  source.each_line {|line|
    total_lines += 1

    next if line.strip.empty?
    next if map.include? line
    map.push(line)
  }

  dest = File.open(dest, 'w+:UTF-8') unless dest.is_a? File

  map.each {|line| dest.puts line}

  puts "Total strings: #{total_lines}".green
  puts "Unique strings: #{map.length}".green
end

def help
  puts 'String Helper Tool © Shegorat'.yellow
  puts 'string_tool.exe sync_spaces <source_dir1> <source_dir2> <output_dir>'.yellow
  puts 'string_tool.exe dict_translate <dict_dir1> <dict_dir2> <source_dir> <dest_dir>'.yellow
  puts 'string_tool.exe json_beautify <source> <output>'.yellow
  puts 'string_tool.exe json_minify <source> <output>'.yellow
  puts 'string_tool.exe map_replace <source> <output> <hash> <reverse>'.yellow
  puts 'string_tool.exe drop <source> <output> <mask>'.yellow
  puts 'string_tool.exe inject <source> <output> <mask> <replacement>'.yellow
  puts 'string_tool.exe csv_drop <source> <output> <...params>'.yellow
  puts 'string_tool.exe csv_inject <source> <output> <...params>'.yellow
  puts 'csv_drop, csv_inject params:'.yellow
  puts "\t-cn=\"<column names>\" -- drop only selected columns".yellow
  puts "\t-pc                  -- print column names".yellow
  puts "\t-i                   -- info only".yellow
  puts "\t-dl=<delimiter>      -- column delimiter".yellow
  puts "\t-fq                  -- force row quotes".yellow
  puts "\t-lp                  -- liberal parsing".yellow
  puts 'string_tool.exe dedup <source> <output>'.yellow
end

if __FILE__ == $0

  case ARGV[0]
    when 'sync_spaces'
      sync_spaces(ARGV[1], ARGV[2], ARGV[3])
    when 'dict_translate'
      dict_translate(ARGV[1], ARGV[2], ARGV[3], ARGV[4])
    when 'json_beautify'
      json_beautify(ARGV[1], ARGV[2])
    when 'json_minify'
      json_minify(ARGV[1], ARGV[2])
    when 'map_replace'
      map_replace(ARGV[1], ARGV[2], ARGV[3], ARGV[4] == 'reverse')
    when 'drop'
      drop(ARGV[1], ARGV[2], ARGV[3])
    when 'inject'
      inject(ARGV[1], ARGV[2], ARGV[3], ARGV[4])
    when 'csv_drop'
      csv_drop(ARGV[1], ARGV[2], ARGV[3..ARGV.length])
    when 'csv_inject'
      csv_inject(ARGV[1], ARGV[2], ARGV[3..ARGV.length])
    when 'dedup'
      dedup(ARGV[1], ARGV[2])
    else
      puts 'Unknown command'.red
      help
      exit -1
  end

  puts 'All OK'.green
end
