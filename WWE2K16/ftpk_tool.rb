require 'bindata'
require 'pathname'

class ZString < BinData::Record
  endian  :little

  uint32  :len
  string  :val, :read_length => :len

  def str=(v)
    self.val = v
    self.len = v.length
  end
end

class DataItem < BinData::Record
  endian  :little

  uint64  :data_size
  string  :raw, :read_length => :data_size

  def data=(r)
    self.raw = r
    self.data_size = self.raw.num_bytes
  end
end

class FTPK < BinData::Record
  endian  :little

  string  :id, :length => 4, :assert_value => 'FTPK'
  uint32  :item_count, :value => lambda { names.count / 2 }

  array   :names, :type => ZString, :initial_length => lambda { self.item_count * 2 }
  array   :debug, :type => ZString, :initial_length => lambda { self.item_count * 2 }
  array   :data, :type => DataItem, :initial_length => lambda { self.item_count * 2 }

  def read_file(fname)
    f = File.open(fname, 'rb')
    self.read(f)
  end

  def write_file(fname)
    f = File.open(fname, 'w+b')
    self.write(f)
  end

  def update(folder, index)
    path = folder + names[index].val

    if path.exist?
      print('Update: ', path, "\n")
      f = File.open(path, 'rb')
      str = f.read
      self.data[index].data = str
    end
  end

  def extract(folder)
    folder = Pathname(folder)
    folder.mkdir unless folder.exist?

    (self.item_count * 2).times do |i|
      path = folder + names[i].val
      print('Extract: ', path, "\n")
      f = File.open(path, 'w+b')
      f.write(self.data[i].raw)
    end
  end

  def import(folder)
    folder = Pathname(folder)

    if folder.exist?
      (self.item_count * 2).times do |i|
        self.update(folder, i)
      end
    end
  end
end

# TODO: Refactor code

if __FILE__ == $0
  if ARGV.count < 3
    puts 'Too few arguments'
    exit
  end

  if ARGV[0] == 'extract'
    puts 'Extracting data...'

    ftpk = FTPK.new
    ftpk.read_file(ARGV[1])
    ftpk.extract(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Importing data...'

    ftpk = FTPK.new
    ftpk.read_file(ARGV[1])
    ftpk.import(ARGV[2])
    ftpk.write_file(ARGV[3])
  else
    puts 'Imvalid command...'
    exit
  end

  puts 'All OK...'
end