require 'bindata'
require_relative '../custom_strings'
require_relative '../dds_format'

class RFI < BaseInterface
  endian  :little

  uint32  :tag
  uint32  :height
  uint32  :width
  uint32  :flags

  count_bytes_remaining :bytes_remaining
  string      :data, :read_length => :bytes_remaining

  def make_dds(io)
    dds = DDS_Texture.new
    dds.data = self.data
    dds.tex_height = self.height
    dds.tex_width = self.width
    dds.dxt1!

    io = File.open(io, 'w+b') unless io.is_a?(File)
    dds.write(io)
  end
end

# TODO: Finish code

rfi = RFI.new
rfi.read_bin('/home/oem/Test/titlefont.rfi')
rfi.make_dds('/home/oem/Test/titlefont.dds')