require 'bindata'
require 'json'
require_relative '../custom_strings'

REPLACE_HASH = Hash["\r" => '<CR>', "\n" => '<LF>']

def drop_text(src, dest)
  src = File.open(src, 'r:UTF-8') unless src.is_a?(File)
  dest = File.open(dest, 'w+:UTF-8') unless dest.is_a?(File)

  json = JSON.load(src)

  puts 'Drop "engineConfig" table'
  json['engineConfig'].each_key {|key|
    dest.puts hash_replace(json['engineConfig'][key], REPLACE_HASH)
  }

  puts 'Drop "meta" table'
  # drop meta
  json['meta'].each {|val|
    # name
    dest.puts hash_replace(val['name'], REPLACE_HASH) if val['name'] != nil

    # desc
    dest.puts hash_replace(val['desc'], REPLACE_HASH) if val['desc'] != nil

    # description ?? why??
    dest.puts hash_replace(val['description'], REPLACE_HASH) if val['description'] != nil

    # item attributes
    val['attributes'].each {|attr|
      dest.puts hash_replace(attr, REPLACE_HASH) if attr != nil
    } if val['attributes'] != nil
  }

  puts 'Drop "section" tables'

  json['map'].keys.each {|key|
    json["section_#{key}"].each {|item|
      next if item['type'] == nil
      next unless %w(text comment mapText).include? item['type']

      dest.puts hash_replace(item['value'], REPLACE_HASH) if item['value'] != nil

    } if json["section_#{key}"] != nil
  } if json['map'] != nil

end

def import(src, raw, dest)
  src = File.open(src, 'r:UTF-8') unless src.is_a? File
  raw = File.open(raw, 'r:UTF-8') unless raw.is_a? File

  json = JSON.load(src)

  puts 'Insert "engineConfig" table'

  json['engineConfig'].each_key {|key|
    json['engineConfig'][key] = hash_replace(raw.readline.chomp, REPLACE_HASH, true)
  }

  puts 'Insert "meta" values'

  # inject meta
  json['meta'].each {|val|

    # name
    val['name'] = hash_replace(raw.readline.chomp, swap_hash(REPLACE_HASH), true) if val['name'] != nil

    # desc
    val['desc'] = hash_replace(raw.readline.chomp, swap_hash(REPLACE_HASH), true) if val['desc'] != nil

    # description
    val['description'] = hash_replace(raw.readline.chomp, swap_hash(REPLACE_HASH), true) if val['description'] != nil

    # item attributes
    if val['attributes'] != nil
      arr = Array[]
      val['attributes'].each {arr << hash_replace(raw.readline.chomp, swap_hash(REPLACE_HASH), true)}
      val['attributes'] = arr
    end
  }

  puts 'Insert "section" values'

  json['map'].keys.each {|key|
    json["section_#{key}"].each {|item|
      next if item['type'] == nil
      next unless %w(text comment mapText).include? item['type']

      item['value'] = hash_replace(raw.readline.chomp, swap_hash(REPLACE_HASH), true)
    } if json["section_#{key}"] != nil
  } if json['map'] != nil

  src.close

  dest = File.open(dest, 'w+:UTF-8') unless dest.is_a? File
  dest.write(JSON.pretty_generate(json))
end

def help
  puts 'The Warlock of Firetop, (*.txt) © Shegorat'
  puts 'Usage:'
  puts 'wofmos.exe -export <source_file> <text_file>'
  puts 'wofmos.exe -import <source_file> <text_file> <new_file>'
  puts 'wofmos.exe -import <source_file> <text_file> -- overwrite source file'
end

if __FILE__ == $0

  if ARGV.length < 3
    puts 'Invalid arguments'
    help
    exit
  end

  case ARGV[0]
    when '-export'
      # drop json data
      drop_text(ARGV[1], ARGV[2])
    when '-import'
      # restore data to json
      import(ARGV[1], ARGV[2], ARGV[3] == nil ? ARGV[1] : ARGV[3])
    else
      help
      exit
  end


end