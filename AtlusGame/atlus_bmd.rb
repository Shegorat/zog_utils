# Atlus Game, (*.bmd) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class FLW < BaseInterface

end

class BMD < BaseInterface
  endian  :little

  uint32  :version
  uint32  :content_size, value: -> {self.num_bytes}
  string  :id, length: 4
  uint32  :dummy
  uint32  :real_data, value: -> {content_size - appendix_size}
  uint32  :appendix_size, value: -> {appendix.num_bytes}
  uint32  :offsets_count, value: -> {names.length}
  uint32  :flags

  array   :offsets, initial_length: :offsets_count do
    uint32  :dummy
    uint32  :offset, value: -> {names.rel_offset + names[index].rel_offset - 32} # 32 - header size
  end

  uint32  :appendix_offset, value: -> {appendix.rel_offset - 32}

  array   :names, initial_length: :offsets_count, byte_align: 16 do
    UTF8String  :title, length: 24
    uint32      :flags
    uint32      :u1
    uint32      :name_size, value: -> {message.num_bytes + 6}
    uint32      :flags2
    uint16      :flags3
    UTF8Stringz :message, read_length: -> {name_size - 6}

    string  :al_item, length: -> {align(names[index].al_item.rel_offset, 4)}
  end

  string  :appendix, read_length: :appendix_size

  def export(io)
    self.names.each do |name|
      io.puts normalize_string(name.message)
    end
  end

  def import(io)
    self.offsets_count.times do |i|
      str = io.readline.b
      self.names[i].message = denormalize_string(str)
    end
  end
end

def help
  puts 'Atlus Game, (*.bmd) © Shegorat'
  puts 'Usage:'
  puts 'atlus_bmd.exe -export <source_file> <text_file>'
  puts 'atlus_bmd.exe -import <source_file> <text_file> <new_file>'
  puts 'atlus_bmd.exe -import <source_file> <text_file> -- overwrite source file'
end

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      bmd = BMD.new
      bmd.read_bin(ARGV[1])
      bmd.save_text(ARGV[2])
    when '-import'
      puts 'Convert from text to binary.'

      bmd = BMD.new
      bmd.read_bin(ARGV[1])
      bmd.read_text(ARGV[2])
      bmd.save_bin(ARGV[1]) if ARGV[3] == nil
      bmd.save_bin(ARGV[3]) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end