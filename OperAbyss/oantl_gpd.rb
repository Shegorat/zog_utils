# Operation Abyss New Tokyo Legacy, (*.gpd) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class GPDString < BinData::Record
  endian  :little

  uint16  :str_len, value: -> { str.num_bytes - 1 }
  UTF8Stringz :str, read_length: -> {  str_len + 1 }

  def get
    self.str
  end

  def set(s)
    self.str = s
  end

end

class GPD < BaseInterface
  endian  :little

  uint32  :u1
  uint32  :u2
  uint32  :u3
  uint32  :name_count, value: -> { strings.length }
  uint32  :u4

  string  :name, length: 4
  GPDString :id
  GPDString :unk1
  GPDString :unk2
  uint16  :u5
  uint16  :u6
  string  :paramb, length: 5
  uint8   :u7

  array   :strings, initial_length: :name_count do
    GPDString :idx
    GPDString :title
    GPDString :message
  end

  def export(io)
    self.strings.each do |str|
      io.puts normalize_string(str.message.get)
    end
  end

  def import(io)
    self.name_count.times do |i|
      str = io.readline
      self.strings[i].message.set(denormalize_string(str))
    end
  end
end

def help
  puts 'Operation Abyss New Tokyo Legacy, (*.gpd) © Shegorat'
  puts 'Usage:'
  puts 'oantl_pgd.exe -export <source_file> <text_file>'
  puts 'oantl_pgd.exe -import <source_file> <text_file> <new_file>'
  puts 'oantl_pgd.exe -import <source_file> <text_file> -- overwrite source file'
end

if __FILE__ == $0

  if ARGV.length < 3
    help
    exit
  end

  case ARGV[0]
    when '-export'
      puts 'Convert from binary to text.'

      file = GPD.new
      file.read_bin(ARGV[1])
      file.save_text(ARGV[2])
    when '-import'
      puts 'Convert from text to binary.'

      file = GPD.new
      file.read_bin(ARGV[1])
      file.read_text(ARGV[2])
      file.save_bin(ARGV[1]) if ARGV[3] == nil
      file.save_bin(ARGV[3]) unless ARGV[3] == nil
    else
      puts 'Unknown command'
      exit
  end

  puts 'All OK'
end