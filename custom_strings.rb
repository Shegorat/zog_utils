require 'bindata'

class WStringz < BinData::Stringz
  def assign(val)
    super(val.force_encoding('UTF-16LE'))
  end

  def snapshot
    result = _value
    trim_and_zero_terminate(result).chomp("\u0000".encode('UTF-16LE'))
  end

  private

  def value_to_binary_string(val)
    binary_string(trim_and_zero_terminate(val))
  end

  def read_and_return_value(io)
    max_length = eval_parameter(:max_length)
    str = ''.encode('UTF-16LE')
    i = 0
    ch = nil
    terminator = "\u0000".encode('UTF-16LE')

    # read until zero byte or we have read in the max number of bytes
    while ch != terminator && i != max_length
      #break if io.eof?
      ch = io.readbytes(2).force_encoding('UTF-16LE')
      str << ch
      i += 1
    end

    trim_and_zero_terminate(str)
  end

  def trim_and_zero_terminate(str)
    #result = binary_string(str)
    result = str.force_encoding('UTF-16LE')
    truncate_after_first_zero_byte!(result)
    trim_to!(result, eval_parameter(:max_length))
    append_zero_byte_if_needed!(result)
    result
  end

  def truncate_after_first_zero_byte!(str)
    re = Regexp.new("([^\u0000]*\u0000).*".encode('UTF-16LE'))
    str.sub!(re, '\1')
  end

  def trim_to!(str, max_length = nil)
    if max_length
      max_length = 1 if max_length < 1
      str.slice!(max_length)
      d = "\u0000".encode('UTF-16LE')
      if str.length == max_length && str[-1, 1] != d
        str[-1, 1] = d
      end
    end
  end

  def append_zero_byte_if_needed!(str)
    d = "\u0000".encode('UTF-16LE')
    if str.length == 0 || str[-1, 1] != d
      str << d
    end
  end
end

class WString < BinData::String
  def assign(val)
    super(val.force_encoding('UTF-16LE'))
  end

  def snapshot
    result = _value
    trim_and_zero_terminate(result).chomp("\u0000".encode('UTF-16LE'))
  end

  private

  def value_to_binary_string(val)
    binary_string(trim_and_zero_terminate(val))
  end

  def read_and_return_value(io)
    max_length = eval_parameter(:max_length)
    str = ''.encode('UTF-16LE')
    i = 0
    ch = nil
    terminator = "\u0000".encode('UTF-16LE')

    # read until zero byte or we have read in the max number of bytes
    while ch != terminator && i != max_length
      #break if io.eof?
      ch = io.readbytes(2).force_encoding('UTF-16LE')
      str << ch
      i += 1
    end

    trim_and_zero_terminate(str)
  end

  def trim_and_zero_terminate(str)
    #result = binary_string(str)
    result = str.force_encoding('UTF-16LE')
    truncate_after_first_zero_byte!(result)
    trim_to!(result, eval_parameter(:max_length))
    append_zero_byte_if_needed!(result)
    result
  end

  def truncate_after_first_zero_byte!(str)
    re = Regexp.new("([^\u0000]*\u0000).*".encode('UTF-16LE'))
    str.sub!(re, '\1')
  end

  def trim_to!(str, max_length = nil)
    if max_length
      max_length = 1 if max_length < 1
      str.slice!(max_length)
      d = "\u0000".encode('UTF-16LE')
      if str.length == max_length && str[-1, 1] != d
        str[-1, 1] = d
      end
    end
  end

  def append_zero_byte_if_needed!(str)
    d = "\u0000".encode('UTF-16LE')
    if str.length == 0 || str[-1, 1] != d
      str << d
    end
  end
end

class UTF8String < BinData::String

  optional_parameter :read_until

  def snapshot
    super.force_encoding('UTF-8')
  end

  def read_and_return_value(io)
    return super(io) unless has_parameter?(:read_until)

    # eval read_until
    val = ''
    loop do
      #break if io.eof?

      b = io.readbytes(1)
      val << b
      variables = {value: val.b}
      break if eval_parameter(:read_until, variables)
    end
    val
  end
end

class UTF8Stringz < BinData::Stringz
  def snapshot
    super.force_encoding('UTF-8')
  end
end

class ISOString < BinData::String
  def snapshot
    super.force_encoding('ISO-8859-1')
  end
end

class ISOStringz < BinData::Stringz
  def snapshot
    super.force_encoding('ISO-8859-1')
  end
end


def normalize_string(str)
  encoding = str.encoding
  exp = Regexp.new("(\r|\n)".encode(encoding))
  r = "\r".encode(encoding)
  cr = '<CR>'.encode(encoding)
  n = "\n".encode(encoding)
  lf = '<LF>'.encode(encoding)

  str.gsub(exp, r => cr, n => lf).strip
end

def denormalize_string(str)
  encoding = str.encoding
  exp = Regexp.new('(<CR>|<LF>)'.encode(encoding))
  r = "\r".encode(encoding)
  cr = '<CR>'.encode(encoding)
  n = "\n".encode(encoding)
  lf = '<LF>'.encode(encoding)

  str.strip.gsub(exp, cr => r, lf => n)
end

def put_endian(io, endian = :little)
  case endian
  when :little
    io << "\uFEFF".encode('UTF-16LE')
  when :big
    io << "\uFEFF".encode('UTF-16BE')
  else
    io << "\uFEFF".encode('UTF-16LE')
  end
end

def bool2int(b)
  b ? 1 : 0
end

class BaseInterface < BinData::Record
  def align(pos, k)
    (pos.div(k) + bool2int(pos.modulo(k) > 0)) * k - pos
  end

  def align_pos(pos, k)
    (pos.div(k) + bool2int(pos.modulo(k) > 0)) * k
  end

  def import(io)
    raise 'Method import not implemented'
  end

  def export(io)
    raise 'Method export not implemented'
  end

  def read_text(io, cp = 'UTF-8')
    io = File.open(io, "rb:#{cp}") unless io.is_a?(File)
    import(io)
  end

  def save_text(io, cp = 'UTF-8')
    io = File.open(io, "w+b:#{cp}") unless io.is_a?(File)
    export(io)
  end

  def read_bin(io)
    io = File.open(io, 'rb') unless io.is_a?(File)
    self.read(io)
  end

  def save_bin(io)
    io = File.open(io, 'w+b') unless io.is_a?(File)
    self.write(io)
  end
end

# TODO: make full hash map replace

def unescape(s)
  eval %Q{"#{s}"}
end

def create_replace_map(source, reverse = false)
  hash = Hash.new('')
  source.each_line do |line|
    params = line.b.split('=>').flatten.map { |match| unescape(match.strip).b }
    next if params.length != 2
    params.reverse! if reverse
    hash[params[0]] = params[1]
  end
  hash

end

def swap_hash(map)
  hash = {}
  map.each_key { |key|
    hash[map[key]] = key
  }
  hash
end

def hash_replace(source, map, reverse = false)
  return source if map.empty?

  rex = Regexp.new "(#{ map.keys.join('|') })", nil, 'n'
  source.b.gsub(rex) { |match|
    if map.key?(match) and !map[match].empty?
      map[match]
    else
      if !reverse
        d = match.bytes.map { |a| 'x%2X' % a }.join('')
        "<#{d}>"
      else
        rex2 = Regexp.new 'x([\dA-Z]+)', nil, 'n'
        match.scan(rex2).flatten.map { |a| a.hex.chr }.join
      end
    end
  }.force_encoding('UTF-8')
end