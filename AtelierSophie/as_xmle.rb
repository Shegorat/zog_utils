require 'bindata'
require 'zlib'
require_relative '../custom_strings'

class XMLE < BaseInterface
  endian :big

  uint32  :flags
  uint32  :uncompressed_size

  uint32  :u1
  uint32  :u2

  count_bytes_remaining :bytes_remaining
  string      :data, :read_length => :bytes_remaining

  def dump
    unp = Zlib.inflate(self.data)
  end
end


xmle = XMLE.new
xmle.read_bin('/home/oem/Test/item_status.xml.e')

puts xmle.dump