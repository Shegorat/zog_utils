# WWE2K15 (text type1) © Shegorat

require 'bindata'
require_relative '../custom_strings'

class TypeString < BinData::Record
  endian  :little

  uint32  :dummy
  uint32  :item_count, value: -> { strings.length }

  array  :offsets, initial_length: :item_count do
    uint32  :str_offset, value: -> { strings.rel_offset + strings[index].rel_offset }
    uint32  :str_size, value: -> { strings[index].num_bytes }
    uint32  :str_id
  end

  array  :strings, :initial_length => :item_count do
    UTF8Stringz :str_value
  end

  def item_off(index)
    self.strings[index].abs_offset
  end

  def item_size(index)
    self.strings[index].num_bytes
  end

  def update_offsets
    offsets[0].str_offset = strings[0].abs_offset
    (1...item_count).each do |i|
      offsets[i].str_offset = offsets[i - 1].str_offset + offsets[i - 1].str_size
    end
  end

  def save_to_text(io)
    item_count.times do |i|
      str = strings[i].gsub(/([\r\n])/, "\r" => '<CR>', "\n" => '<LF>').strip
      io.printf("%d = %s\n", offsets[i].str_id, str )
    end
  end

  def save_to_plain(io)
    item_count.times do |i|
      str = strings[i].gsub(/([\r\n])/, "\r" => '<CR>', "\n" => '<LF>').strip
      io.printf("%s\n", str )
    end
  end

  def save_to_plain2(io)
    item_count.times do |i|
      str = strings[i].gsub(/([\r\n])/, "\r" => '<CR>', "\n" => '<LF>').strip
      io.printf("%s\n", str ) unless str.empty?
    end
  end

  def load_from_text(io)
    io.each do |line|
      strs = line.split('=')
      id = strs[0].strip.to_i
      str = strs[1].strip.gsub(/(<CR>|<LF>)/, '<CR>' => "\r", '<LF>' => "\n")
      strings << str
      offsets << {:str_offset => 0, :str_size => strings.last.num_bytes, :str_id => id}
    end

    update_offsets
  end

  def import_from_plain(io)
    self.item_count.times do |i|
      line = io.readline
      str = line.strip.gsub(/(<CR>|<LF>)/, '<CR>' => "\r", '<LF>' => "\n")
      strings[i] = str
    end

    update_offsets
  end

  def import_from_plain2(io)
    self.item_count.times do |i|
      next if strings[i].empty?

      line = io.readline
      str = line.strip.gsub(/(<CR>|<LF>)/, '<CR>' => "\r", '<LF>' => "\n")
      strings[i] = str
    end

    update_offsets
  end

  def read_bin(fname)
    io = File.open(fname, 'rb')
    self.read(io)
  end

  def read_text(fname)
    io = File.open(fname, 'r:UTF-8')
    load_from_text(io)
  end

  def read_plain(fname)
    io = File.open(fname, 'r:UTF-8')
    import_from_plain(io)
  end

  def read_plain2(fname)
    io = File.open(fname, 'r:UTF-8')
    import_from_plain2(io)
  end

  def save_text(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_text(io)
  end

  def save_plain(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_plain(io)
  end

  def save_plain2(fname)
    io = File.open(fname, 'w+:UTF-8')
    save_to_plain2(io)
  end

  def save_bin(fname)
    io = File.open(fname, 'w+b')
    self.write(io)
  end
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    puts 'Too few arguments'
    exit
  end

  if ARGV[0] == 'to_txt'
    puts 'Convert from binary to text..'

    ts = TypeString.new
    ts.read_bin(ARGV[1])
    ts.save_text(ARGV[2])
  elsif ARGV[0] == 'to_bin'
    puts 'Convert from text to binary..'

    ts = TypeString.new
    ts.read_text(ARGV[1])
    ts.save_bin(ARGV[2])
  elsif ARGV[0] == 'to_plain'
    puts 'Convert from binary to plain text..'

    ts = TypeString.new
    ts.read_bin(ARGV[1])
    ts.save_plain(ARGV[2])
  elsif ARGV[0] == 'to_plain2'
    puts 'Convert from binary to plain text..'

    ts = TypeString.new
    ts.read_bin(ARGV[1])
    ts.save_plain2(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Import plain text to binary..'

    ts = TypeString.new
    ts.read_bin(ARGV[1])
    ts.read_plain(ARGV[2])
    ts.save_bin(ARGV[3])
  elsif ARGV[0] == 'import2'
    puts 'Import plain text to binary..'

    ts = TypeString.new
    ts.read_bin(ARGV[1])
    ts.read_plain2(ARGV[2])
    ts.save_bin(ARGV[3])
  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end