require 'bindata'
require_relative '../custom_strings'

def str_prep(str)
  str.gsub(/([\r\n])/, "\r" => '<CR>', "\n" => '<LF>')
end

def rev_str(str)
  str.gsub(/(<CR>|<LF>)/, '<CR>' => "\r", '<LF>' => "\n")
end

class XL_V1 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint32  :msg_off#, :value => lambda {strings[index].abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8Stringz :str
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i] = off
      off += val.num_bytes
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str)) unless str.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.msg_count.times do |n|
      self.strings[n] = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].empty?
    end
  end
end

class XL_V2 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint32  :id
    uint32  :msg_off#, :value => lambda {strings[index].abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8Stringz :str
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i].msg_off = off
      off += val.num_bytes
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str)) unless str.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.msg_count.times do |n|
      self.strings[n] = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].empty?
    end
  end
end

class XL_V3 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint32  :msg_off1#, :value => lambda {strings[index].str1.abs_offset - self.cbsize}
    uint32  :msg_off2#, :value => lambda {strings[index].str2.abs_offset - self.cbsize}
    uint32  :msg_off3#, :value => lambda {strings[index].str3.abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8Stringz :str1
    UTF8Stringz :str2
    UTF8Stringz :str3
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i].msg_off1 = off
      off += val.str1.num_bytes
      self.offsets[i].msg_off2 = off
      off += val.str2.num_bytes
      self.offsets[i].msg_off3 = off
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1))
      io.puts(str_prep(str.str2))
      io.puts(str_prep(str.str3))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1)) unless str.str1.empty?
      io.puts(str_prep(str.str2)) unless str.str2.empty?
      io.puts(str_prep(str.str3)) unless str.str3.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.msg_count.times do |n|
      self.strings[n].str1 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str1.empty?
      self.strings[n].str2 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str2.empty?
      self.strings[n].str3 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str3.empty?
    end
  end
end

class XL_V5 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint32  :msg_off1#, :value => lambda {strings[index].str1.abs_offset - self.cbsize}
    uint32  :msg_off2#, :value => lambda {strings[index].str2.abs_offset - self.cbsize}
    uint32  :msg_off3#, :value => lambda {strings[index].str3.abs_offset - self.cbsize}
    uint32  :msg_off4#, :value => lambda {strings[index].str4.abs_offset - self.cbsize}
    uint32  :msg_off5#, :value => lambda {strings[index].str5.abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8Stringz :str1
    UTF8Stringz :str2
    UTF8Stringz :str3
    UTF8Stringz :str4
    UTF8Stringz :str5
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i].msg_off1 = off
      off += val.str1.num_bytes
      self.offsets[i].msg_off2 = off
      off += val.str2.num_bytes
      self.offsets[i].msg_off3 = off
      off += val.str3.num_bytes
      self.offsets[i].msg_off4 = off
      off += val.str4.num_bytes
      self.offsets[i].msg_off5 = off
      off += val.str5.num_bytes
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1))
      io.puts(str_prep(str.str2))
      io.puts(str_prep(str.str3))
      io.puts(str_prep(str.str4))
      io.puts(str_prep(str.str5))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1)) unless str.str1.empty?
      io.puts(str_prep(str.str2)) unless str.str2.empty?
      io.puts(str_prep(str.str3)) unless str.str3.empty?
      io.puts(str_prep(str.str4)) unless str.str4.empty?
      io.puts(str_prep(str.str5)) unless str.str5.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.msg_count.times do |n|
      self.strings[n].str1 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str1.empty?
      self.strings[n].str2 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str2.empty?
      self.strings[n].str3 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str3.empty?
      self.strings[n].str4 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str4.empty?
      self.strings[n].str5 = rev_str(io.readline.tr("\r\n", '')) unless self.strings[n].str5.empty?
    end
  end
end

class XL_V6 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint16  :u1
    uint16  :u2
    uint16  :u3
    uint16  :u4
    uint16  :u5
    uint32  :msg_off#, :value => lambda {strings[index].abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8String :str
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i].msg_off = off
      off += val.num_bytes
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str)) unless str.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.msg_count.times do |n|
      strings[n] = rev_str(io.readline.tr("\r\n", '')) unless strings[n].empty?
    end
  end
end

class XL_V8 < BinData::Record
  endian  :little

  uint16  :msg_count
  uint16  :arr_size#, :value => lambda { self.item_count * 4 } # 20
  uint32  :cbsize   # 24

  array   :align1, :type => :uint8, :initial_length => lambda { self.cbsize - 16 }

  array   :offsets, :initial_length => lambda {itemcount} do
    uint32  :msg_off1#, :value => lambda {strings[index].str1.abs_offset - self.cbsize}
    uint32  :msg_off2#, :value => lambda {strings[index].str2.abs_offset - self.cbsize}
    uint32  :msg_off3#, :value => lambda {strings[index].str3.abs_offset - self.cbsize}
    uint32  :msg_off4#, :value => lambda {strings[index].str4.abs_offset - self.cbsize}
    uint32  :msg_off5#, :value => lambda {strings[index].str5.abs_offset - self.cbsize}
    uint32  :msg_off6#, :value => lambda {strings[index].str6.abs_offset - self.cbsize}
    uint32  :msg_off7#, :value => lambda {strings[index].str7.abs_offset - self.cbsize}
    uint32  :msg_off8#, :value => lambda {strings[index].str8.abs_offset - self.cbsize}
  end

  array   :strings, :initial_length => lambda {itemcount} do
    UTF8Stringz :str1
    UTF8Stringz :str2
    UTF8Stringz :str3
    UTF8Stringz :str4
    UTF8Stringz :str5
    UTF8Stringz :str6
    UTF8Stringz :str7
    UTF8Stringz :str8
  end

  def itemcount
    self.msg_count
  end

  def update
    off = self.strings.abs_offset - self.cbsize
    self.strings.each_with_index do |val, i|
      self.offsets[i].msg_off1 = off
      off += val.str1.num_bytes
      self.offsets[i].msg_off2 = off
      off += val.str2.num_bytes
      self.offsets[i].msg_off3 = off
      off += val.str3.num_bytes
      self.offsets[i].msg_off4 = off
      off += val.str4.num_bytes
      self.offsets[i].msg_off5 = off
      off += val.str5.num_bytes
      self.offsets[i].msg_off6 = off
      off += val.str6.num_bytes
      self.offsets[i].msg_off7 = off
      off += val.str7.num_bytes
      self.offsets[i].msg_off8 = off
      off += val.str8.num_bytes
    end
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1))
      io.puts(str_prep(str.str2))
      io.puts(str_prep(str.str3))
      io.puts(str_prep(str.str4))
      io.puts(str_prep(str.str5))
      io.puts(str_prep(str.str6))
      io.puts(str_prep(str.str7))
      io.puts(str_prep(str.str8))
    end
  end

  def plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File
    self.strings.each do |str|
      io.puts(str_prep(str.str1)) unless str.str1.empty?
      io.puts(str_prep(str.str2)) unless str.str2.empty?
      io.puts(str_prep(str.str3)) unless str.str3.empty?
      io.puts(str_prep(str.str4)) unless str.str4.empty?
      io.puts(str_prep(str.str5)) unless str.str5.empty?
      io.puts(str_prep(str.str6)) unless str.str6.empty?
      io.puts(str_prep(str.str7)) unless str.str7.empty?
      io.puts(str_prep(str.str8)) unless str.str8.empty?
    end
  end

  def import(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    msg_count.times do |n|
      strings[n].str1 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str1.empty?
      strings[n].str2 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str2.empty?
      strings[n].str3 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str3.empty?
      strings[n].str4 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str4.empty?
      strings[n].str5 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str5.empty?
      strings[n].str6 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str6.empty?
      strings[n].str7 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str7.empty?
      strings[n].str8 = rev_str(io.readline.tr("\r\n", '')) unless strings[n].str8.empty?
    end
  end
end

class XL_TXT < BinData::Record
  endian  :little

  string  :id, :length => 4, :assert => "XL\x13\x00"

  uint16  :self_size, :value => lambda { self.num_bytes % 65536}

  uint16  :item_count

  choice  :xl, :selection => :item_count do
    XL_V1 1
    XL_V2 2
    XL_V3 3
    XL_V5 5
    XL_V6 6
    XL_V8 8
  end

  def read_bin(io)
    io = File.open(io, 'rb') unless io === File

    self.read(io)
  end

  def read_plain(io)
    io = File.open(io, 'r:UTF-8') unless io === File

    self.xl.import(io)
    self.xl.update
  end

  def save_plain(io)
    io = File.open(io, 'w+:UTF-8') unless io === File

    self.xl.plain(io)
  end

  def dump(io)
    io = File.open(io, 'w+:UTF-8') unless io === File

    self.xl.dump(io)
  end

  def save_bin(io)
    io = File.open(io, 'w+b') unless io === File

    self.write(io)
  end
end

# TODO: Refactor code

if __FILE__ == $0

  if ARGV.count < 3
    puts 'Too few arguments'
    exit
  end

  if ARGV[0] == 'to_plain'
    puts 'Convert from binary to plain text..'

    xl = XL_TXT.new
    xl.read_bin(ARGV[1])
    xl.save_plain(ARGV[2])
  elsif ARGV[0] == 'import'
    puts 'Import plain text to binary..'

    xl = XL_TXT.new
    xl.read_bin(ARGV[1])
    xl.read_plain(ARGV[2])
    xl.save_bin(ARGV[3])
  else
    puts 'Invalid command'
    exit
  end

  puts 'All OK..'
end