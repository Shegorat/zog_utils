require 'bindata'

# noinspection RubyResolve
class MCD_header < BinData::Record
  endian  :little

  uint32  :off_str_table
  uint32  :cnt_str_table
  uint32  :off_sym_codes
  uint32  :cnt_sym_codes
  uint32  :off_sym_table
  uint32  :cnt_sym_table
  uint32  :off_fnt_table
  uint32  :cnt_fnt_table
  uint32  :off_unk2
  uint32  :cnt_unk2
end

# noinspection RubyResolve
class MCD_str < BinData::Record
  endian  :little

  uint32  :s_offset
  uint32  :s_entrycount
  uint32  :s_unk3
  uint32  :s_id
end

# noinspection RubyResolve
class MCD_str_entry < BinData::Record
  endian  :little

  uint32  :se_offset
  uint32  :se_blockcount
  uint32  :se_dummy
  uint32  :se_totallength
  uint32  :se_fontid
end

# noinspection RubyResolve
class MCD_str_entry_block < BinData::Record
  endian  :little

  uint32   :be_offset
  uint32   :be_prev_block_len
  uint32   :be_length
  uint32   :be_length2
  uint64   :be_line_height
end

# noinspection RubyResolve
class MCD_str_entry_block_data < BinData::Record
  endian   :little

  uint16   :bd_val
end

# noinspection RubyResolve
class MCD_code < BinData::Record
  endian  :little

  uint16  :ct_fontid
  uint16  :ct_char
  uint32  :ct_code
end

# noinspection RubyResolve
class MCD_symbol < BinData::Record
  endian  :little

  uint32  :sm_texid
  float   :sm_i1
  float   :sm_i2
  float   :sm_i3
  float   :sm_i4
  float   :sm_w
  float   :sm_h
  uint32  :sm_z1
  uint32  :sm_z2
  uint32  :sm_z3
end

# noinspection RubyResolve
class MCD_font < BinData::Record
  endian  :little

  uint32  :ft_fontid
  float   :ft_glyph_width
  float   :ft_glyph_height
  float   :ft_kern_y
  float   :ft_kern_x
end

# noinspection RubyResolve
class MCD_unk_v2 < BinData::Record
  endian  :little

  uint32  :ut2_unk1
  uint32  :ut2_unk2
  string  :ut2_name, :length => 32
end

class MCD_unk_extra < BinData::Record
  endian  :little

  string  :ut2_name, :length => 32
end

# noinspection RubyResolve
class MCD_unk_v1 < BinData::Record
  endian  :little

  uint32  :ut2_unk1
  uint32  :ut2_unk2
end

if __FILE__ == $0

  # ARGV.each do |arg|
  #   puts arg
  # end

  # make min args
  if ARGV.count < 4
    puts "Not enough arguments\n"
    puts "Usage: mcd_ver <option> <infile> <outfile> <deltafile>\n"
    puts "Options:\n"
    puts "\tmgrr2trms - ...\n"
    puts "\ttrms2mgrr - ...\n"
    exit 0
  end


  flag = 0 if ARGV[0] == 'mgrr2trms'
  flag = 1 if ARGV[0] == 'trms2mgrr'
  infile = ARGV[1]
  outfile = ARGV[2]
  deltafile = ARGV[3]

  hdr = MCD_header.new
  str_tbl = BinData::Array.new(:type => MCD_str)
  str_entry_tbl = BinData::Array.new(:type => MCD_str_entry)
  str_block_tbl = BinData::Array.new(:type => MCD_str_entry_block)
  str_block_data= BinData::Array.new(:type => MCD_str_entry_block_data)
  code_tbl = BinData::Array.new(:type => MCD_code)
  symbol_tbl = BinData::Array.new(:type => MCD_symbol)
  font_tbl = BinData::Array.new(:type => MCD_font)
  unk1_tbl = BinData::Array.new(:type => MCD_unk_v1)
  unk2_tbl = BinData::Array.new(:type => MCD_unk_v2)
  unk2_ext = BinData::Array.new(:type => MCD_unk_extra)

  File.open(infile, 'rb') do |io|
    hdr.read(io)

    off = hdr.off_str_table
    hdr.cnt_str_table.times do
      io.seek(off)

      str = MCD_str.new.read(io)
      str_tbl.push(str)
      off = io.tell

      off2 = str.s_offset
      str.s_entrycount.times do
        io.seek(off2)

        str_entry = MCD_str_entry.new.read(io)
        str_entry_tbl.push(str_entry)
        off2 = io.tell

        off3 = str_entry.se_offset
        str_entry.se_blockcount.times do
          io.seek(off3)

          str_block = MCD_str_entry_block.new.read(io)
          str_block_tbl.push(str_block)
          off3 = io.tell

          io.seek(str_block.be_offset)
          if flag == 1
            cntr = str_block.be_length
          else
            cntr = str_block.be_length * 2
          end
          cntr += 1 if (str_block.be_length2 != str_block.be_length)
          cntr.times do
            data = MCD_str_entry_block_data.new.read(io)
            str_block_data.push(data)
          end
        end
      end
    end

    io.seek(hdr.off_sym_codes)
    hdr.cnt_sym_codes.times do
      code_tbl.push(MCD_code.new.read(io))
    end

    io.seek(hdr.off_sym_table)
    hdr.cnt_sym_table.times do
      symbol_tbl.push(MCD_symbol.new.read(io))
    end

    io.seek(hdr.off_fnt_table)
    hdr.cnt_fnt_table.times do
      font_tbl.push(MCD_font.new.read(io))
    end

    io.seek(hdr.off_unk2)
    hdr.cnt_unk2.times do
      if flag == 1
        unk2 = MCD_unk_v2.new.read(io)
        unk2_tbl.push(unk2)
        unk1 = MCD_unk_v1.new
        unk1.ut2_unk1 = unk2.ut2_unk1
        unk1.ut2_unk2 = unk2.ut2_unk2
        unk1_tbl.push(unk1)
        extr = MCD_unk_extra.new
        extr.ut2_name = unk2.ut2_name
        unk2_ext.push(extr)
      else
        unk1 = MCD_unk_v1.new.read(io)
        unk1_tbl.push(unk1)
        unk2 = MCD_unk_v2.new
        unk2.ut2_unk1 = unk1.ut2_unk1
        unk2.ut2_unk2 = unk1.ut2_unk2
        unk2_tbl.push(unk2)
      end
    end
  end

  if flag == 1
    File.open(deltafile, 'wb') do |io|
      unk2_ext.each do |obj|
        obj.write(io)
      end
    end
  else
    File.open(deltafile, 'rb') do |io|
      hdr.cnt_unk2.times do |i|
        unk2_ext.push(MCD_unk_extra.new.read(io))
        unk2_tbl[i].ut2_name = unk2_ext[i].ut2_name
      end
    end
  end
  puts 'Read data... OK'

  #base_off_hdr = 0
  base_off_block_data = hdr.num_bytes
  base_off_str = base_off_block_data + str_block_data.num_bytes
  base_off_str += 4 if flag == 0
  base_off_str_ends = base_off_str + str_tbl.num_bytes + 4 + str_entry_tbl.num_bytes + str_block_tbl.num_bytes
  base_off_str_ends += 8 if flag == 0

  base_off_str_entry = base_off_str + str_tbl.num_bytes + 4
  base_off_str_block = base_off_str_entry + str_entry_tbl.num_bytes + 4

  base_off_codes = base_off_str_ends
  base_off_symbols = base_off_codes + code_tbl.num_bytes
  base_off_symbols += 4 if flag == 0
  base_off_fonts = base_off_symbols + symbol_tbl.num_bytes
  base_off_fonts += 4 if flag == 0
  base_off_unk = base_off_fonts + font_tbl.num_bytes
  base_off_unk += 4 if flag == 0

  str_entry_cnt = 0
  str_block_cnt = 0
  str_data_cnt = 0

  hdr.off_str_table = base_off_str
  hdr.off_sym_codes = base_off_codes
  hdr.off_sym_table = base_off_symbols
  hdr.off_fnt_table = base_off_fonts
  hdr.off_unk2 = base_off_unk

  size_str_block = str_block_tbl[0].num_bytes
  size_str_entry = str_entry_tbl[0].num_bytes

  #File.open("E:\\new.mcd", "wb") do |io|
  File.open(outfile, 'wb') do |io|
    hdr.write(io)

    io.seek(base_off_block_data)
    str_block_data.each do |obj|
      obj.write(io)
    end
    puts 'Write header... OK'
    io.seek(base_off_str)

    if flag == 1 # new MCD => old MCD
      base_off1 = base_off_str
      base_off2 = base_off_str_entry

      str_tbl.each do |str|
        io.seek(base_off1)
        str.s_offset = base_off2
        str.write(io)
        base_off1 = io.tell

        io.seek(base_off2)
        adds_off = str.s_entrycount * size_str_entry

        str.s_entrycount.times do |ik|
          entry = str_entry_tbl[str_entry_cnt + ik]
          entry.se_offset = base_off2 + adds_off
          entry.write(io)
          adds_off += entry.se_blockcount * size_str_block
        end

        str.s_entrycount.times do |ik|
          entry = str_entry_tbl[str_entry_cnt + ik]
          entry.se_blockcount.times do
            block = str_block_tbl[str_block_cnt]
            cnt = (block.be_length - 1) / 2
            block.be_length = cnt
            block.be_length2 = cnt + 1
            block.be_offset = base_off_block_data + str_block_data[str_data_cnt].abs_offset
            block.write(io)
            str_data_cnt += cnt * 2 + 1
            str_block_cnt += 1
          end
        end

        str_entry_cnt += str.s_entrycount
        base_off2 = io.tell
      end

    else # old MCD => new MCD
      str_tbl.each do |str|
        str.s_offset = base_off_str_entry + str_entry_tbl[str_entry_cnt].abs_offset
        str.write(io)
        str_entry_cnt += str.s_entrycount
      end


      io.seek(base_off_str_entry)
      str_entry_tbl.each do |str_entry|
        str_entry.se_offset = base_off_str_block + str_block_tbl[str_block_cnt].abs_offset
        str_entry.write(io)
        str_block_cnt += str_entry.se_blockcount
      end

      io.seek(base_off_str_block)
      str_block_tbl.each do |block|
        cnt = block.be_length*2 + 1
        block.be_length = cnt
        block.be_length2 = cnt
        block.be_offset = base_off_block_data +  str_block_data[str_data_cnt].abs_offset
        block.write(io)
        str_data_cnt += block.be_length
      end
    end
    puts 'Write strings... OK'

    io.seek(base_off_codes)
   code_tbl.each do |code|
     code.write(io)
   end
    puts 'Write codes... OK'

   io.seek(base_off_symbols)
   symbol_tbl.each do |sym|
     sym.write(io)
   end
    puts 'Write symbols... OK'

   io.seek(base_off_fonts)
   font_tbl.each do |fnt|
     fnt.write(io)
   end
    puts 'Write fonts... OK'

   io.seek(base_off_unk)
   if flag == 1
     unk1_tbl.each do |unk|
       unk.write(io)
     end
   else
     unk2_tbl.each do |unk|
       unk.write(io)
     end
   end
  end
  puts 'Write unknown... OK'

end